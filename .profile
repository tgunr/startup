#!/bin/bash 
echo .profile $1 $2 $3  
#logger "bash: .profile..."
# 	File:		$Id: .profile,v 1.6 2003/04/25 06:41:36 davec Exp $	
# 		
# 	Contains:	BASH initialization code
#
# This file gets executed only when bash is a login shell.

# If bash is the initial login shell then we have ~/.bash_login source
# in ~/.bashrc.  When bash is not a login shell the ~/.bashrc is always
# executed.

# If [t]csh is the initial shell then either ~/.cshrc or ~/.login will
# invoke bash replacing [t]csh as the current shell and thus still
# causing ~/.bashrc to be executed.

# This script defines some basic commands involved with shell nesting
# and the common help mechanism defined for use with related setup
# scripts.  Those scripts are used to further define the environment.
# There's ~/.userstartup which is used to establish some initial
# defaults, aliases, etc.  It's executed from here first prior to
# any scripts in the ~/.startup folder.  The scripts in the
# ~/.startup should be used to establish project specific 
# conventions (see ~/.startup/.skeleton_statup_file for
# documentation - it's never executed, it's just a readme file
# illustrating what real .startup scripts should look like).

#------------------------------------------------------------------------------------------#
#
## These folllowing two aliases are defined first to control set -x tracing.
## In certain places, like here, we always want to disable tracing.  Use
## __push_trace to save trace state and turn it off and __pop_trace to restore
## tracing if it was on at the time of the __push_trace.  As just mentioned,
## these are only used in selected places and then where we know we are doing a
## top-level command since this push is not done to a stack.

### NOTICE!!!
# Do not place anything in .bashrc which will echo back to terminal or many non-interactive
# things will not work properly. Things like `ssh me@somehost.com ls some/path' would return
# extra text which is probably not what you desire. Any thing you want echoed back should be
# placed here in .bash_profile 
export __INTERACTIVE_SHELL__=1

# # What's the point of doing all this if we're not interactive?
# if [ "$TERM" != "dumb" ] ; then
# 	if [ "`echo $- | grep i`" = "" ]; then
# 		return
# 	fi
# fi

# stty erase "^?" intr "^C" kill "^U" -decctlq
# TERM=`tset -s`
# export TERM

# Display some system info if this is the initial bash login...

if [ -x /usr/bin/sw_vers ]; then
	sw_vers
else
	uname -srvm
fi

#--Green Prompt

# export PS1="\n\[\033[0;32m\][\[\033[0;37m\]\t\[\033[0;32m\]][\[\033[0;37m\]\u@\h\[\033[0;32m\]]\n[\[\033[1;37m\]\w\[\033[0;32m\]]\[\033[1;32m\]$\[\033[0m\] "

#--Purple
export PS1="\n\[\033[0;35m\][\[\033[0;37m\]\t\[\033[0;35m\]][\[\033[0;37m\]\u@\h\[\033[0;35m\]]\n[\[\033[1;37m\]\w\[\033[0;35m\]]\[\033[1;35m\]$\[\033[0m\] "

#--Blue 
#export PS1="\n\[\033[0;34m\][\[\033[0;37m\]\t\[\033[0;34m\]][\[\033[0;37m\]\u@\h\[\033[0;34m\]]\n[\[\033[1;37m\]\w\[\033[0;34m\]]\[\033[1;34m\]$\[\033[0m\] "

unset f
export __startup_completed=1
echo "User startup complete"

#------------------------------------------------------------------------------------------#

# Wait until all .startup scripts are run to do the cd to set the 1st prompt.
# We need to wait since startups could define ~userid prefixes for pathnames
# with define_known_directory calls and we might be "in" one of those directories
# when .bashrc is executed.

cd "$PWD"

export PATH="$HOME/.cargo/bin:$PATH"
TZ='America/Chicago'; export TZ
