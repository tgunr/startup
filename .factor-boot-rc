! Copyright (C) 2012 PolyMicro Systems.
! See http://factorcode.org/license.txt for BSD license.

USING: accessors alien.c-types alien.data assocs byte-arrays byte-vectors
 colors compiler.units definitions
 effects effects.parser endian fonts fry
 help.vocabs hints io io.encodings.utf8 io.files io.standard-paths
 kernel kernel.private lexer locals macros make
 math math.order math.parser parser prettyprint
 regexp sequences sequences.private serialize slots.private sorting
 splitting.extras strings.parser system tools.scaffold tr
 typed ui.clipboards ui.commands ui.gestures ui.text ui.tools.browser
 ui.tools.common ui.tools.listener variables vocabs.loader vocabs.parser words
 debugger init vectors namespaces syntax.terse io.styles ;


IN: listener
: history@ ( history -- seq )
    elements>> ;

: .history ( -- )
    get-listener input>> history>> elements>>
    [ string>> ] map .
    ;

listener-gadget "history"
"The listener's history can be scrolled from the keyboard."
{
    { T{ key-down f { C+ } "y" } .history }
} define-command-map


IN: scratchpad
: >usings ( vocabs -- )
    [ name>> ] map
    "" [ 1 + 6 mod 0 = [ "\n" append ] when  " " append append ] reduce-index
    "USING: " prepend " ;" append
    clipboard get set-clipboard-contents ;

IN: namespaces
SYMBOL: factor-history-path
factor-history-path [ "~/.factor-history" ] initialize

: set-history ( -- )
    factor-history-path get utf8
    file-lines
    [  34 swap [ = not ] with filter 
       input boa ] map >vector
    get-listener input>> history>> over >>elements
    swap length >>index
    drop
    ;

: startup-banner ( -- )
    {    "To rebuild factor enter in terminal:" 
         "make"
         "factor -i=boot.unix-x86.64.image"
         "To Startup factor UI"
         "\"unix-x86.64\" make-image"
    } [ print ] each ;


: save-history ( -- )
    factor-history-path get utf8
    [ get-listener input>> history>> elements>>
      [ string>> . ] each
    ] with-file-appender
    ;

: my-shutdown ( -- )
    [ save-history  ] try
;

: my-startup ( -- )
    [ scaffold-emacs
      set-history
      startup-banner
    ] try
    ;

[ my-shutdown ] "my-shutdown" add-shutdown-hook
[ my-startup ] "my-startup" add-startup-hook
! STARTUP-HOOK: my-startup
! SHUTDOWN-HOOK: my-shutdown

IN: vocabs
TUPLE: vocab < identity-tuple
name words
main help
source-loaded? docs-loaded? overlay-loaded? ;

: vwords ( -- ) manifest get current-vocab>> words>> keys [ . ] each ;

USE: io.directories
USE: splitting
USE: sets
USE: continuations

IN: vocabs.loader

CONSTANT: default-vocab-roots {
    "resource:core"
    "resource:basis"
    "resource:extra"
    "resource:work"
    "resource:overlays"
}

: prepend-overlays-dir ( vocab str/f -- path )
    [ vocab-name "." split ] dip
    [ [ dup last ] dip append suffix "resource:overlays" prefix ] when*
    "/" join ;

: vocab-overlay-path ( vocab -- path/f )
    vocab-name ".private" ?tail drop
    dup ".factor" prepend-overlays-dir vocab-append-path ;

SYMBOL: require-when-vocabs
require-when-vocabs [ HS{ } clone ] initialize

SYMBOL: require-when-table
require-when-table [ V{ } clone ] initialize

: load-conditional-requires ( vocab -- )
    vocab-name require-when-vocabs get in? [
        require-when-table get [
            [ [ lookup-vocab dup [ source-loaded?>> +done+ = ] when ] all? ] dip
            [ require ] curry when
        ] assoc-each
    ] when ;

: (load-source-finish) ( vocab quote -- )
    [ +parsing+ >>source-loaded? ] dip
    [ parse-file ] [ [ ] ] if*
    [ % ] [ call( -- ) ] if-bootstrapping
    +done+ >>source-loaded?
    load-conditional-requires ;

: load-source-1 ( vocab -- )
    dup check-vocab-hook get call( vocab -- )
    [
        dup vocab-source-path
        (load-source-finish)
    ] [ ] [ f >>source-loaded? ] cleanup ;

: load-source ( vocab -- )
    dup check-vocab-hook get call( vocab -- )
    [
        f >>main
        +parsing+ >>source-loaded?
        dup vocab-source-path [ parse-file ] [ [ ] ] if*
        [ +parsing+ >>source-loaded? ] dip
        [ % ] [ call( -- ) ] if-bootstrapping
        +done+ >>source-loaded?
        load-conditional-requires
    ] [ ] [ f >>source-loaded? ] cleanup ;

: load-overlay ( vocab -- )
    "overlays" get-global [ 
    dup check-vocab-hook get call( vocab -- )
    [ dup vocab-overlay-path dup [
          dup file-exists? [
              dup [ "Overlay: " over append print ] when
              (load-source-finish)
          ] [ drop f >>overlay-loaded? drop ] if
      ] [  drop f >>overlay-loaded? drop ] if
    ] [ ] [ f >>overlay-loaded? ] cleanup
    ]
    [ drop ] if ;

: load-overlays ( -- )
    "resource:overlays" recursive-directory-files
    [ "." split last "factor" = ] filter
    [ run-file ] each ;

IN: vocabs.loader.private
M: vocab (require)
    dup source-loaded?>> +parsing+ eq? [
        dup source-loaded?>> [ dup load-source ] unless
        dup overlay-loaded?>> [ dup load-overlay ] unless
        dup docs-loaded?>> [ dup load-docs ] unless
    ] unless drop ;

