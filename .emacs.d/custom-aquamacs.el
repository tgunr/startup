(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-use-header-line t)
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 350.1 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(auto-fill-function nil t)
 '(auto-save-interval 0)
 '(backup-directory-alist (quote (("*" . "~/emacs.d/backups"))))
 '(before-save-hook (quote (time-stamp)))
 '(csv-align-style (quote auto))
 '(csv-invisibility-default t)
 '(cua-rectangle-mark-key (kbd "<C-S-return>"))
 '(current-language-environment "UTF-8")
 '(cursor-type (quote box))
 '(custom-enabled-themes (quote (tango-dark)))
 '(custom-safe-themes
   (quote
    ("ee8f819d520143c44a2e2217d5c5a06da46b8c77d9b390b2cdf76dbf9c176cbc" "dc758223066a28f3c6ef6c42c9136bf4c913ec6d3b710794252dc072a3b92b14" "5d7663d3ba79d30da483081b79d4e90850841448fea16ee1f602b06a68f72955" default)))
 '(custom-theme-directory "~/.emacs.d/themes")
 '(debug-on-error nil)
 '(default-frame-alist
    (quote
     ((font-backend ns)
      (fontsize . 0)
      (font . "-apple-Menlo-medium-normal-normal-*-12-*-*-*-m-0-iso10646-1")
      (left-fringe . 6)
      (right-fringe . 15)
      (menu-bar-lines . 1)
      (tool-bar-lines . 0)
      (fringe)
      (modeline . t)
      (cursor-type . box)
      (vertical-scroll-bars . right)
      (internal-border-width . 0)
      (width . 115)
      (height . 67))))
 '(dired-find-subdir t)
 '(dired-hide-details-hide-symlink-targets nil)
 '(dired-listing-switches "-la")
 '(dired-load-hook
   (quote
    ((lambda nil
       (load "dired-x")
       (setq dired-guess-shell-gnutar "gtar")
       (setq dired-comparefiles-command "comparefiles")
       (dired-x-bind-find-file)
       (setq dired-x-hands-off-my-keys nil)))))
 '(dired-ls-F-marks-symlinks t)
 '(dired-no-confirm
   (quote
    (byte-compile chgrp chmod chown compress copy hardlink move shell symlink touch uncompress)))
 '(dired-x-hands-off-my-keys t)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(el-get-status-file "~/emacs/el-get/.status.el")
 '(el-get-user-package-directory "~/.emacs.d/el-get/el-get-init-files")
 '(factor-mode-use-fuel t)
 '(fci-rule-character-color "#d9d9d9")
 '(fci-rule-color "#d9d9d9")
 '(font-lock-keywords-case-fold-search t t)
 '(fringe-indicator-alist
   (quote
    ((truncation left-truncation right-truncation)
     (continuation left-continuation right-continuation)
     (overlay-arrow . right-triangle)
     (up . up-arrow)
     (down . down-arrow)
     (top top-left-angle top-right-angle)
     (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle)
     (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
     (empty-line . empty-line)
     (unknown . question-mark))) t)
 '(fuel-autodoc-eval-using-form-p t)
 '(fuel-edit-word-method (quote frame))
 '(fuel-help-bookmarks
   (quote
    (("collections" "Collections" article)
     ("pathname" "pathname" word)
     ("handbook" "Factor handbook" article)
     ("tuples" "Tuples" article)
     ("tools.scaffold" "Scaffold tool" article)
     ("quotations" "Quotations" article)
     ("handbook-tools-reference" "Developer tools" article)
     ("help.home" "Factor documentation" article)
     ("browser-gadget" "browser-gadget" word))))
 '(fuel-listener-use-other-window nil)
 '(fuel-listener-window-allow-split nil)
 '(fuel-mode-autodoc-p nil)
 '(fuel-scaffold-developer-name "Dave Carlton")
 '(gdb-show-main t)
 '(global-linum-mode t)
 '(graphviz-dot-auto-indent-on-braces t)
 '(graphviz-dot-view-command "/Applications/Graphviz.app/Contents/MacOS/Graphviz %s")
 '(graphviz-dot-view-edit-command t)
 '(indent-tabs-mode nil)
 '(linum-delay t)
 '(linum-eager nil)
 '(minibuffer-auto-raise t)
 '(minibuffer-depth-indicate-mode t)
 '(mouse-wheel-scroll-amount (quote (1 ((shift) . 0.5) ((hyper) . 0.2))))
 '(ns-command-modifier (quote alt))
 '(ns-control-modifier (quote control))
 '(ns-function-modifier (quote hyper))
 '(ns-right-alternate-modifier (quote hyper))
 '(ns-right-command-modifier (quote super))
 '(obof-same-frame-regexps (quote (" SPEEDBAR")))
 '(one-buffer-one-frame-mode t nil (aquamacs-frame-setup))
 '(package-selected-packages
   (quote
    (frames-only-mode csv python-mode cargo demangle-mode rust-mode visual-regexp visual-regexp-steroids)))
 '(py--imenu-create-index-p t)
 '(vc-follow-symlinks t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

