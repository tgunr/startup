(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ack-command "/usr/local/bin/ack ")
 '(ag-executable "/usr/local/bin/ag")
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 308 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(blink-matching-delay 2)
 '(cursor-type (quote box))
 '(custom-enabled-themes (quote (tango-dark)))
 '(custom-safe-themes
   (quote
    ("5d7663d3ba79d30da483081b79d4e90850841448fea16ee1f602b06a68f72955" "013e87003e1e965d8ad78ee5b8927e743f940c7679959149bbee9a15bd286689" "6c9ddb5e2ac58afb32358def7c68b6211f30dec8a92e44d2b9552141f76891b3" default)))
 '(debug-on-error nil)
 '(default-frame-alist
    (quote
     ((cursor-type . box)
      (vertical-scroll-bars . right)
      (internal-border-width . 0)
      (mouse-color . "black")
      (cursor-color . "Red")
      (modeline . t)
      (fringe)
      (background-mode . light)
      (tool-bar-lines . 1)
      (menu-bar-lines . 1)
      (right-fringe . 8)
      (left-fringe . 1)
      (background-color . "White")
      (foreground-color . "Black")
      (font . "-*-Inconsolata-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1")
      (fontsize . 0)
      (font-backend mac-ct))))
 '(delete-by-moving-to-trash t)
 '(dired-guess-shell-gnutar "gtar")
 '(dired-recursive-copies (quote always))
 '(dired-recursive-deletes (quote always))
 '(dired-x-hands-off-my-keys nil)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(fuel-listener-use-other-window t)
 '(fuel-listener-window-allow-split nil)
 '(fuel-mode-autodoc-p nil)
 '(fuel-mode-autohelp-p t)
 '(fuel-mode-stack-p t)
 '(graphviz-dot-auto-indent-on-braces t)
 '(graphviz-dot-dot-program "/usr/local/bin/dot")
 '(graphviz-dot-view-edit-command t)
 '(grep-files-aliases
   (quote
    (("all" . "* .[!.]* ..?*")
     ("el" . "*.el")
     ("ch" . "*.[ch]")
     ("c" . "*.c")
     ("cc" . "*.cc *.cxx *.cpp *.C *.CC *.c++")
     ("cchh" . "*.cc *.[ch]xx *.[ch]pp *.[CHh] *.CC *.HH *.[ch]++")
     ("hh" . "*.hxx *.hpp *.[Hh] *.HH *.h++")
     ("h" . "*.h")
     ("l" . "[Cc]hange[Ll]og*")
     ("m" . "[Mm]akefile*")
     ("tex" . "*.tex")
     ("texi" . "*.texi")
     ("asm" . "*.[sS]")
     ("m" . "*.m")
     ("mh" . "*.[mh]")
     ("mm" . "*.mm")
     ("mmh" . "*.mm *.h")
     ("objc" . "*.[cmh] *.mm")
     ("swift" . "*.swift"))))
 '(indent-tabs-mode t)
 '(initial-scratch-message
   ";; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.
(emacs-version)
")
 '(magit-log-section-arguments (quote ("--color" "--decorate" "-n256")))
 '(ns-alternate-modifier (quote meta))
 '(ns-command-modifier (quote alt))
 '(ns-function-modifier (quote none))
 '(ns-right-alternate-modifier (quote super))
 '(ns-right-command-modifier (quote hyper))
 '(ns-right-control-modifier (quote none))
 '(ns-tool-bar-display-mode (quote both) t)
 '(ns-tool-bar-size-mode (quote regular) t)
 '(one-buffer-one-frame-mode t nil (aquamacs-frame-setup))
 '(package-archives
(quote
 (("melpa" . "https://melpa.org/packages/")
  ("gnu" . "http://elpa.gnu.org/packages/"))))
 '(package-enable-at-startup nil)
'(package-selected-packages
(quote
 (magit magit-gitflow ialign s sed-mode dash el-search cmake-font-lock cmake-ide cmake-mode cmake-project lang-refactor-perl perlbrew arduino-mode osx-browse osx-plist osx-trash magit-lfs magit-rockstar ag hl-anything ssh ssh-config-mode ssh-tunnels adjust-parens flylisp scad-mode scad-preview apache-mode markdown-mode plantuml-mode elpy python-info python-mode forth-mode floobits visual-regexp apples-mode)))
'(safe-local-variable-values
(quote
 ((eval progn
	(c-set-offset
	 (quote innamespace)
	 0)
	(c-set-offset
	 (quote topmost-intro)
	 0)
	(c-set-offset
	 (quote cpp-macro-cont)
	 (quote ++))
	(c-set-offset
	 (quote case-label)
	 (quote +))
	(c-set-offset
	 (quote member-init-intro)
	 (quote ++))
	(c-set-offset
	 (quote statement-cont)
	 (quote ++))
	(c-set-offset
	 (quote arglist-intro)
	 (quote ++)))
  (indicate-empty-lines . t)
  (factor-block-offset . 4)
  (factor-indent-level . 4))))
 '(tabbar-mode t nil (tabbar))
 '(tramp-completion-reread-directory-timeout 20)
 '(tramp-connection-timeout 30)
 '(tramp-default-host "pve")
 '(tramp-default-host-alist (quote (("adb" nil ""))))
 '(tramp-default-method "ssh")
'(tramp-default-method-alist
(quote
 ((nil "%" "smb")
  ("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|macpro\\.local\\)\\'" "\\`root\\'" "sudo")
  (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
  ("\\`ftp\\." nil "ftp"))))
 '(tramp-default-proxies-alist nil)
 '(tramp-default-user "davec")
'(tramp-default-user-alist
(quote
 (("\\`smb\\'" nil nil)
  ("\\`\\(?:fcp\\|krlogin\\|r\\(?:cp\\|emcp\\|sh\\)\\|telnet\\)\\'" nil "davec")
  ("\\`\\(?:ksu\\|su\\(?:do\\)?\\)\\'" nil "root")
  ("\\`\\(?:socks\\|tunnel\\)\\'" nil "davec")
  ("\\`synce\\'" nil nil))))
'(tramp-remote-path
(quote
 (tramp-default-remote-path "/usr/sbin" "/usr/local/bin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin")))
'(tramp-shell-prompt-pattern
"\\(?:^\\|
\\)[^#$%>:
]*#?[#$%>:] *\\(\\[[0-9;]*[a-zA-Z] *\\)*")
 '(tramp-verbose 3)
 '(vc-follow-symlinks t)
 '(visual-line-mode nil t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(conf-unix-mode-default ((t (:inherit default :height 150 :family "Inconsolata"))) t)
 '(custom-theme-choose-mode-default ((t (:inherit special-mode-default :background "#1c0945"))) t)
 '(emacs-lisp-mode-default ((t (:inherit prog-mode-default :height 130 :family "Menlo"))) t)
 '(markdown-mode-default ((t (:inherit text-mode-default :height 120 :family "Menlo"))) t)
 '(nxml-mode-default ((t (:inherit text-mode-default :height 130 :family "Menlo"))) t))
