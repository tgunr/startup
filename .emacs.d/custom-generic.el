(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-use-header-line t)
 '(auto-fill-function nil t)
 '(auto-save-interval 0)
 '(backup-directory-alist '(("*" . "~/emacs.d/backups")))
 '(before-save-hook '(time-stamp))
 '(csv-align-style 'auto)
 '(csv-invisibility-default t)
 '(cua-rectangle-mark-key (kbd "<C-S-return>"))
 '(current-language-environment "UTF-8")
 '(cursor-type 'box)
 '(custom-enabled-themes '(tango-dark))
 '(custom-safe-themes
   '("dc758223066a28f3c6ef6c42c9136bf4c913ec6d3b710794252dc072a3b92b14" "5d7663d3ba79d30da483081b79d4e90850841448fea16ee1f602b06a68f72955" default))
 '(custom-theme-directory "~/.emacs.d/themes")
 '(debug-on-error nil)
 '(dired-find-subdir t)
 '(dired-listing-switches "-la")
 '(dired-load-hook
   '((lambda nil
       (load "dired-x")
       (setq dired-guess-shell-gnutar "gtar")
       (setq dired-comparefiles-command "comparefiles")
       (dired-x-bind-find-file)
       (setq dired-x-hands-off-my-keys nil))))
 '(dired-no-confirm
   '(byte-compile chgrp chmod chown compress copy hardlink move shell symlink touch uncompress))
 '(dired-x-hands-off-my-keys t)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(el-get-status-file "~/emacs/el-get/.status.el")
 '(el-get-user-package-directory "~/.emacs.d/el-get/el-get-init-files")
 '(factor-mode-use-fuel t)
 '(fci-rule-character-color "#d9d9d9")
 '(fci-rule-color "#d9d9d9")
 '(font-lock-keywords-case-fold-search t t)
 '(fringe-indicator-alist
   '((truncation left-truncation right-truncation)
     (continuation left-continuation right-continuation)
     (overlay-arrow . right-triangle)
     (up . up-arrow)
     (down . down-arrow)
     (top top-left-angle top-right-angle)
     (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle)
     (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
     (empty-line . empty-line)
     (unknown . question-mark)) t)
 '(fuel-autodoc-eval-using-form-p t)
 '(fuel-edit-word-method 'frame)
 '(fuel-help-bookmarks
   '(("collections" "Collections" article)
     ("pathname" "pathname" word)
     ("handbook" "Factor handbook" article)
     ("tuples" "Tuples" article)
     ("tools.scaffold" "Scaffold tool" article)
     ("quotations" "Quotations" article)
     ("handbook-tools-reference" "Developer tools" article)
     ("help.home" "Factor documentation" article)
     ("browser-gadget" "browser-gadget" word)))
 '(fuel-listener-use-other-window nil)
 '(fuel-listener-window-allow-split nil)
 '(fuel-scaffold-developer-name "Dave Carlton")
 '(gdb-show-main t)
 '(global-linum-mode t)
 '(graphviz-dot-auto-indent-on-braces t)
 '(graphviz-dot-view-command "/Applications/Graphviz.app/Contents/MacOS/Graphviz %s")
 '(graphviz-dot-view-edit-command t)
 '(indent-tabs-mode nil)
 '(linum-delay t)
 '(linum-eager nil)
 '(minibuffer-auto-raise t)
 '(minibuffer-depth-indicate-mode t)
 '(mouse-wheel-scroll-amount '(1 ((shift) . 0.5) ((hyper) . 0.2)))
 '(ns-command-modifier 'alt)
 '(ns-control-modifier 'control)
 '(ns-function-modifier 'hyper)
 '(ns-right-alternate-modifier 'hyper)
 '(ns-right-command-modifier 'super)
 '(ns-tool-bar-display-mode 'both t)
 '(ns-tool-bar-size-mode 'regular t)
 '(ns-use-qd-smoothing t)
 '(package-archives
   '(("melpa-stable" . "https://stable.melpa.org/packages/")
     ("gnu" . "http://elpa.gnu.org/packages/")
     ("marmalade" . "http://marmalade-repo.org/packages/")))
 '(package-selected-packages '(visual-regexp-steroids))
 '(special-display-buffer-names '("*info*" "*Help*"))
 '(special-display-frame-alist '((width . 105) (height . 57)))
 '(split-height-threshold 80)
 '(split-width-threshold 160)
 '(tab-width 4)
 '(tabbar-mode t)
 '(tramp-completion-reread-directory-timeout 30)
 '(tramp-connection-timeout 10)
 '(tramp-default-host-alist '(("adb" nil "")))
 '(tramp-default-method "ssh")
 '(tramp-default-method-alist
   '(("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|pro\\.local\\)\\'" "\\`root\\'" "sudo")
     (nil "%" "smb")
     (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
     ("\\`ftp\\." nil "ftp")))
 '(tramp-default-proxies-alist nil)
 '(tramp-default-user "davec")
 '(tramp-default-user-alist
   '(("\\`smb\\'" nil nil)
     ("\\`\\(?:fcp\\|krlogin\\|r\\(?:cp\\|emcp\\|sh\\)\\|telnet\\)\\'" nil "davec")
     ("\\`\\(?:ksu\\|su\\(?:do\\)?\\)\\'" nil "root")
     ("\\`\\(?:socks\\|tunnel\\)\\'" nil "davec")
     ("\\`synce\\'" nil nil)))
 '(tramp-remote-path
   '(tramp-default-remote-path "/usr/sbin" "/usr/local/bin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"))
 '(truncate-lines t)
 '(vc-follow-symlinks t)
 '(visible-bell t)
 '(visual-line-mode nil t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

