;;; Generated package description from ido-grid-mode.el  -*- no-byte-compile: t -*-
(define-package "ido-grid-mode" "1.1.5" "Display ido-prospects in the minibuffer in a grid." '((emacs "24.4")) :commit "8bbd66e365d4f6f352bbb17673be5869ab26d7ab" :authors '(("Tom Hinton")) :maintainer '("Tom Hinton" . "t@larkery.com") :keywords '("convenience") :url "https://github.com/larkery/ido-grid-mode.el")
