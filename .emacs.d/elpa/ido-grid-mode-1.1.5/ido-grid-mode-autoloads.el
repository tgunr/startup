;;; ido-grid-mode-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ido-grid-mode" "ido-grid-mode.el" (0 0 0 0))
;;; Generated autoloads from ido-grid-mode.el

(defvar ido-grid-mode nil "\
Non-nil if Ido-Grid mode is enabled.
See the `ido-grid-mode' command
for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `ido-grid-mode'.")

(custom-autoload 'ido-grid-mode "ido-grid-mode" nil)

(autoload 'ido-grid-mode "ido-grid-mode" "\
Makes ido-mode display candidates in a grid.

This is a minor mode.  If called interactively, toggle the
`Ido-Grid mode' mode.  If the prefix argument is positive, enable
the mode, and if it is zero or negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `(default-value \\='ido-grid-mode)'.

The mode's hook is called both when the mode is enabled and when
it is disabled.

\(fn &optional ARG)" t nil)

(register-definition-prefixes "ido-grid-mode" '("ido-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ido-grid-mode-autoloads.el ends here
