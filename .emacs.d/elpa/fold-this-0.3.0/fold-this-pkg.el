(define-package "fold-this" "0.3.0" "Just fold this region please" 'nil :commit "90b41d7b588ab1c3295bf69f7dd87bf31b543a6a" :authors
  '(("Magnar Sveen" . "magnars@gmail.com"))
  :maintainers
  '(("Magnar Sveen" . "magnars@gmail.com"))
  :maintainer
  '("Magnar Sveen" . "magnars@gmail.com")
  :keywords
  '("convenience"))
;; Local Variables:
;; no-byte-compile: t
;; End:
