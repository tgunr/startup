;;; ido-yes-or-no-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ido-yes-or-no" "ido-yes-or-no.el" (0 0 0 0))
;;; Generated autoloads from ido-yes-or-no.el

(defvar ido-yes-or-no-mode nil "\
Non-nil if Ido-Yes-Or-No mode is enabled.
See the `ido-yes-or-no-mode' command
for a description of this minor mode.")

(custom-autoload 'ido-yes-or-no-mode "ido-yes-or-no" nil)

(autoload 'ido-yes-or-no-mode "ido-yes-or-no" "\
Use ido for `yes-or-no-p'.

This is a minor mode.  If called interactively, toggle the
`Ido-Yes-Or-No mode' mode.  If the prefix argument is positive,
enable the mode, and if it is zero or negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `(default-value \\='ido-yes-or-no-mode)'.

The mode's hook is called both when the mode is enabled and when
it is disabled.

\(fn &optional ARG)" t nil)

(register-definition-prefixes "ido-yes-or-no" '("ido-yes-or-no-p"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ido-yes-or-no-autoloads.el ends here
