(define-package "htmlize" "1.56" "Convert buffer text and decorations to HTML." 'nil :commit "ed220b1f3be1f898e3cfd628196b16d25a65cb5f" :authors
  '(("Hrvoje Niksic" . "hniksic@gmail.com"))
  :maintainers
  '(("Hrvoje Niksic" . "hniksic@gmail.com"))
  :maintainer
  '("Hrvoje Niksic" . "hniksic@gmail.com")
  :keywords
  '("hypermedia" "extensions")
  :url "https://github.com/hniksic/emacs-htmlize")
;; Local Variables:
;; no-byte-compile: t
;; End:
