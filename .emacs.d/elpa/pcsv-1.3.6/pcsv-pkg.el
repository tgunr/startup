;;; Generated package description from pcsv.el  -*- no-byte-compile: t -*-
(define-package "pcsv" "1.3.6" "Parser of csv" 'nil :commit "91599aaba70a8e8593fa2f36165af82cbd35e41e" :authors '(("Masahiro Hayashi" . "mhayashi1120@gmail.com")) :maintainer '("Masahiro Hayashi" . "mhayashi1120@gmail.com") :keywords '("data") :url "https://github.com/mhayashi1120/Emacs-pcsv/raw/master/pcsv.el")
