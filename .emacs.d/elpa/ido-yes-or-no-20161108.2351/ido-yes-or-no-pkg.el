;; -*- no-byte-compile: t; lexical-binding: nil -*-
(define-package "ido-yes-or-no" "20161108.2351"
  "Use Ido to answer yes-or-no questions."
  '((ido-completing-read+ "0"))
  :url "https://github.com/DarwinAwardWinner/ido-yes-or-no"
  :commit "8953eadaaa7811ebc66d8a9eb7ac43f38913ab59"
  :revdesc "8953eadaaa78"
  :keywords '("convenience" "completion" "ido"))
