;; Object SRecode Map
;; EIEIO PERSISTENT OBJECT
(srecode-map "SRecode Map"
  :file "srecode-map.el"
  :files '(("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/wisent.srt" . wisent-grammar-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/texi.srt" . texinfo-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/template.srt" . srecode-template-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/make.srt" . makefile-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/java.srt" . java-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/el.srt" . emacs-lisp-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/default.srt" . default) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/cpp.srt" . c++-mode))
  :apps '((tests ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/test.srt" . srecode-template-mode)) (getset ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/getset-cpp.srt" . c++-mode)) (ede ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/ede-make.srt" . makefile-mode)) (document ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/doc-java.srt" . java-mode) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/doc-default.srt" . default) ("/Applications/Aquamacs.app/Contents/Resources/etc/srecode/doc-cpp.srt" . c++-mode)))
  )
