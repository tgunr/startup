(deftheme davec
  "Created 2012-11-17.")

;; Color Theme
(require `color-theme)
(defun color-theme-davec ()
  "`color-theme-comidia' with lighter shades.
This theme tries to avoid underlined and italic faces, because
the fonts either look ugly, or do not exist."
  (interactive)
  (color-theme-install
   '(color-theme-davec
	  (
	   (background-color . "grey30")
	   (background-mode . light)
	   (border-color . "black")
	   (cursor-color . "yellow1")
	   (foreground-color . "SteelBlue")
	   (mouse-color . "SteelBlue")
	   (default ((t (:stipple nil :background "Black" :foreground "SteelBlue" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width semi-condensed :family "misc-fixed"))))
	   (bold ((t (:bold t :weight bold))))
	   (bold-italic ((t (:italic t :bold t :slant italic :weight bold))))
	   (border ((t (:background "black"))))
	   (button ((t (:bold t))))
	   (comint-highlight-input ((t (:bold t :weight bold))))
	   (comint-highlight-prompt ((t (:foreground "cyan"))))
	   (cursor ((t (:background "SteelBlue"))))
	   (fixed-pitch ((t (:family "courier"))))
	   (font-lock-builtin-face ((t (:foreground "LightSteelBlue"))))
	   (font-lock-comment-face ((t (:foreground "chocolate1"))))
	   (font-lock-constant-face ((t (:foreground "Aquamarine"))))
	   (font-lock-doc-face ((t (:foreground "LightSalmon"))))
	   (font-lock-doc-string-face ((t (:foreground "LightSalmon"))))
	   (font-lock-function-name-face ((t (:foreground "LightSkyBlue"))))
	   (font-lock-keyword-face ((t (:foreground "Cyan"))))
	   (font-lock-preprocessor-face ((t (:foreground "Aquamarine"))))
	   (font-lock-reference-face ((t (:foreground "LightSteelBlue"))))
	   (font-lock-string-face ((t (:foreground "LightSalmon"))))
	   (font-lock-type-face ((t (:foreground "PaleGreen"))))
	   (font-lock-variable-name-face ((t (:foreground "LightGoldenrod"))))
	   (font-lock-warning-face ((t (:bold t :foreground "Pink" :weight bold))))
	   (fringe ((t (:background "grey10"))))
	   (header-line ((t (:family "neep" :width condensed :box (:line-width 1 :style none) :background "grey20" :foreground "grey90" :box nil))))
	   (highlight ((t (:background "darkolivegreen"))))
	   ;; (isearch ((t (:background "palevioletred2" :foreground "brown4"))))
	   ;; (isearch-lazy-highlight-face ((t (:background "paleturquoise4"))))
	   (isearch ((t (:foreground "black" :background "red"))))
	   (isearch-lazy-highlight-face ((t (:background "white" :foreground "red"))))
	   (italic ((t (:italic t :slant italic))))
	   (menu ((t (nil))))
	   (minibuffer-prompt ((t (:background "black" :foreground "light blue"))))
	   (modeline ((t (:background "lime green" :foreground "gray10" :box (:line-width -1 :style released-button)))))
	   ;; (modeline ((t (:background "Gray10" :foreground "SteelBlue" :box (:line-width 1 :style none) :width condensed :family "neep"))))
	   (modeline-buffer-id ((t (:background "Gray10" :foreground "light blue" :box (:line-width 1 :style none) :width condensed :family "neep"))))
	   (modeline-mousable-minor-mode ((t (:background "Gray10" :foreground "SteelBlue" :box (:line-width 1 :style none) :width condensed :family "neep"))))
	   (modeline-mousable ((t (:background "Gray10" :foreground "SteelBlue" :box (:line-width 1 :style none) :width condensed :family "neep"))))
	   (mouse ((t (:background "SteelBlue"))))
	   (primary-selection ((t (:background "blue3"))))
	   (region ((t (:background "blue3"))))
	   (scroll-bar ((t (:background "grey75"))))
	   (secondary-selection ((t (:background "SkyBlue4"))))
	   (speedbar-button-face ((t (:foreground "green3"))))
	   (speedbar-directory-face ((t (:foreground "light blue"))))
	   (speedbar-file-face ((t (:foreground "cyan"))))
	   (speedbar-highlight-face ((t (:background "sea green"))))
	   (speedbar-selected-face ((t (:foreground "red" :underline t))))
	   (speedbar-tag-face ((t (:foreground "yellow"))))
	   (tool-bar ((t (:background "grey75" :foreground "black" :box (:line-width 1 :style released-button)))))
	   (tooltip ((t (:background "light blue" :foreground "black"))))
	   ;; (tooltip ((t (:background "lightyellow" :foreground "black"))))
	   (trailing-whitespace ((t (:background "red"))))
	   (underline ((t (:underline t))))
	   (variable-pitch ((t (:family "helv"))))
	   (widget-button-face ((t (:bold t :weight bold))))
	   (widget-button-pressed-face ((t (:foreground "red"))))
	   (widget-documentation-face ((t (:foreground "lime green"))))
	   (widget-field-face ((t (:background "dim gray"))))
	   (widget-inactive-face ((t (:foreground "light gray"))))
	   (widget-single-line-field-face ((t (:background "dim gray"))))
	   ))))

;; (custom-theme-set-faces
;;  'davec
;;  '(widget-button ((t (:weight bold :box (:line-width 2 :color "grey75" :style released-button) :foreground "blue"))))
;;  '(factor-font-lock-comment ((nil (:foreground "SlateGray2"))))
;;  '(factor-font-lock-vocabulary-name ((nil (:foreground "violet")))))

(provide-theme 'davec)
