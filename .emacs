;; $Id: .emacs,v 1.20 1994/09/15 01:14:06 davec Exp $
;;
;; Time-stamp: <2024-11-14 16:24:41 davec"~/emacs/aquamacs")
;; ___________________________________________________________________________
;; Aquamacs custom-file warning:
;; Warning: After loading this .emacs file, Aquamacs will also load
;; customizations from `custom-file' (customizations.el). Any settings there
;; will override those made here.
;; Consider moving your startup settings to the Preferences.el file, which
;; is loaded after `custom-file':
;; ~/Library/Preferences/Aquamacs Emacs/Preferences
;; _____________________________________________________________________________

;; (setq max-specpdl-size 5)  ; default is 1000, reduce the backtrace level
;; (setq debug-on-error t)    ; now you should get a backtrace

;;(require 'cl)
(setq byte-compile-warnings '((not cl-functions)))
(defun pbug ()
  "Check parenthesis bugs or similar horrors.

Even with Emacs advanced programming facilities, checking mismatching
parenthesis or missing quote (so called \"pbug\") is no less annoying than
pointer chasing in C.

This function divides the buffer into regions and tries evaluating them one
by one.  It stops at the first region where it fails to evaluate because of
pbug or any other errors.  It sets point and mark (and highlights if
`transient-mark-mode' is on) on the failing region and center its first
line.  \"^def\" is used to define regions.  You may also `eval-region'
right after pbug is done to let lisp parse pinpoint the bug.

No more \"End of file during parsing\" horrors!"
  (interactive)
  (let ((point (point))
        (region-regex "^(def..")
        defs beg end)
    (goto-char (point-min))
    (setq defs (cl-loop while (search-forward-regexp region-regex nil t)
                     collect (point-at-bol)))
    ;; so it evals last definition
    (nconc defs (list (point-max)))
    (setq beg (point-min))
    (while defs
      (goto-char beg)
      (setq end (pop defs))
      ;; to be cool, uncomment these to see pbug doing step by step
      ;; (message "checking pbug from %s to %s..." beg end)
      ;; (sit-for .5)
      (when (eq (condition-case nil
                    (eval-region beg (1- end))
                  (error 'pbug-error))
                'pbug-error)
        (push-mark end 'nomsg 'activate)
        (goto-char beg)
        (recenter)
        (error "a pbug found from %s to %s" beg end))
      (setq beg end))
    (goto-char point)
    (message "no pbug found")))

(defvar emacs-flavor "generic"
"The particular flavor of emacs we are using, either Aquamacs, Emacs app, or generic emacs")

(defun my-custom-load-file ()
  "Loads a custom file depending upon the version of emacs being initialized. Permits different emacs to have thier own settings"
  (interactive)
  (cond
    ; Check for Aquamacs
    ((string-match "Aquamacs" (car (split-string (emacs-version) "\n")))
    (progn
      (setq custom-file "~/.emacs.d/custom-aquamacs.el")
       (add-to-list 'load-path "~/emacs/aquamacs")
       (add-to-list 'load-path "~/emacs/aquamacs/macosx")
       (add-to-list 'load-path "~/emacs/aquamacs/oneonone")
       (add-to-list 'load-path "~/emacs/aquamacs/tabbar")
       (add-to-list 'load-path "~/emacs/aquamacs/util")
       ;; (require 'aquamacs)
      (setq emacs-flavor "aquamacs")))
    ; Check for Emacs app
    ((string-match "porkrind" (car (split-string (emacs-version) "\n")))
     (progn
       (setq custom-file "~/.emacs.d/custom-emacsapp.el")
       (setq emacs-flavor "emacsapp")))
    ((string-match "GNU Emacs" (car (split-string (emacs-version) "\n")))
     (progn
       (setq custom-file "~/.emacs.d/custom-emacsapp.el")
       (setq emacs-flavor "emacsapp")))
    ; Use generic
    (t
     (progn
       (setq custom-file "~/.emacs.d/custom-generic.el")
       (setq emacs-flavor "generic")
       (add-to-list 'load-path "~/emacs/aquamacs")
       )))
  (if (not (file-exists-p custom-file))
      (write-region "" nil custom-file t)
    ))

(my-custom-load-file) 
(load custom-file)

(defmacro with-system (type &rest body)
  "Evaluate BODY if `system-type' equals TYPE."
  (declare (indent defun))
  `(when (eq system-type ',type)
     ,@body))

(defmacro with-flavor (type &rest body)
  "Evaluate BODY if `emacs-flavor' equals TYPE."
  (declare (indent defun))
  `(when (eq emacs-flavor ',type)
     ,@body))

(defun font-name-size (n s) (format "-*-%s-medium-r-*--%s-*-*-*-*-*-*-*" n s))
;; (defun set-mac-font (n s)
;;   (interactive
;;    (let* ((font-name (completing-read "font-name: "
;; 				      (mapcar (lambda (n) (list n n)) (mapcar (lambda (p) (car p)) (x-font-family-list)))
;; 				      nil t))
;; 	  (size (read-number "size: " 12)))
;;      (list font-name size)))
;;   (let ((font-spec (font-name-size n s)))
;;     (if (null (assq 'font default-frame-alist))
;; 	(add-to-list 'default-frame-alist '(font . "")))
;;     (setcdr (assq 'font default-frame-alist) font-spec)
;;     (set-default-font font-spec)))
;; ; (set-mac-font "bitstream vera sans mono" 12)

(defun find-secret (key)
  "Lookup a secret by key name in ~/.secrets which should be mode 600."
  (interactive "DFind secret with key:")
  (setq secrets-file "~/.secrets")
  (setq secrets-buffer (find-buffer-visiting secrets-file))
  (if (eq secrets-buffer nil)
      (setq secrets-buffer (find-file secrets-file)))
    (save-excursion
      (switch-to-buffer secrets-buffer)
      (goto-char (point-min))
      (search-forward key nil t)
      (forward-word)
      (setq bounds (current-word-bounds))
      (setq secret (buffer-substring-no-properties (pop bounds) (pop bounds) ))
      ))


(setq user-mail-address "davec@polymicro.net"
      mail-host-address "mp.polymicro.net"
      user-full-name "Dave Carlton"
      )

(setq gnus-posting-styles
   '((".*" (name "Dave Carlton"))))

(setq mail-sources
      '(
	(file :path "/var/mail/davec")
	(imap :server "mail.polymicro.net"
	      :port 143
	      :user "davec@polymicro.net"
	      :password (find-secret "davec@polymicro.net")
	      :stream network
	      :authenticator login
	      :mailbox "INBOX"
	      :predicate "UNSEEN UNDELETED"
	      :dontexpunge t)
	(pop :server "pop.gmail.com"
	     :port 995
	     :user "polymicro@gmail.com"
	     :password (find-secret "polymicro@gmail.com"))))

;; Configure outbound mail (SMTP)
(setq smtpmail-starttls-credentials '(("mail.polymicro.net" 25 nil nil))
      smtpmail-smtp-server "mail.polymicro.net"
      smtpmail-default-smtp-server "mail.polymicro.net"
      send-mail-function 'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it
      smtpmail-smtp-service 25
      smtpmail-auth-credentials '(
				  ("mail.polymicro.net"
				   25
				   "davec@polymicro.net"
				   (find-secret "davec@polymicro.net"))))
;==============
; These are your IMAP settings
(setq gnus-secondary-select-methods '((nnml "")))
(setq gnus-permanently-visible-groups "mail")

;==============
; Use default web browser
(setq browse-url-browser-function 'browse-url-generic
          browse-url-generic-program "/usr/bin/open")

;; Specifies the form of the "From" header.
(setq message-from-style 'angles)

;; Read only the relevant parts of the active file from the server.
;; This should generally be faster than both the t and nil value.
(setq gnus-read-active-file 'some)

;; Ask the server to list new groups since the last time it checked.
(setq gnus-check-new-newsgroups 'ask-server)

;; Default charset used in non-Mule Emacsen.
(setq message-default-charset 'iso-8859-1)

;; Don't generate a "Sender" header when posting.
(setq message-syntax-checks '((sender . disabled)))

;; If non-nil, generate all possible headers before composing.
(setq message-generate-headers-first t)

;; set variables
(setq ediff-wide-display-p t)
(setq vc-make-backup-files t)
(setq vc-command-messages t)
(setq calendar-time-zone -480)
(setq calendar-standard-time-zone-name "CST")
(put 'iconify-frame 'disabled t)
(setq special-display-buffer-names '("*info*"))
(setq column-number-mode t)
(setq pdf-previewer-program (if (eq system-type 'darwin) "open" "Preview"))

;; Set variables for loaded packages
(setq special-display-frame-alist '((width . 105) (height . 57)))
(setq debug-on-error t)
(setq reb-re-syntax 'string)
(setq user-emacs-directory "~/.emacs.d/")


(defun case-fold-yes ()
  "Set case sensitive"
  (interactive)
  (setq case-fold-search t))

(defun case-fold-no ()
  "Set case sensitive"
  (interactive)
  (setq case-fold-search nil))

(defun mac2unix ()
  (interactive)
  (save-excursion
	(goto-char (point-min))
	(while (search-forward "\r" nil t) (replace-match "\n"))))

(defun my-grep (command-args)
  "Perform 'search` command and switch to its result buffer."
  (interactive
   (progn
	 (let ((default "search -ni " ))
       (list (read-from-minibuffer "Run search (like this): "
								   default
;								   (if current-prefix-arg default default )
								   nil nil 'grep-history default
;								   (if current-prefix-arg nil default)
)))))

  ;; Setting process-setup-function makes exit-message-function work
  ;; even when async processes aren't supported.
  (let* ((compilation-process-setup-function 'grep-process-setup)
		 (buf (compile-internal (if (concat command-args " " null-device)
								  command-args)
								"No more search hits" "grep"
								;; Give it a simpler regexp to match.
								nil grep-regexp-alist))))
  (other-window 1)
;  (toggle-truncate-lines t)
  )

;;;
; Dired stuff
;;;
(require 'dired)
(setq ls-lisp-use-insert-directory-program nil)
(require 'ls-lisp)

(defun dired-stuff ()
  (progn 
	(load "dired-x")
	;; Set dired-x global variables here.  For example:
	(setq dired-guess-shell-gnutar "gtar")
	(setq dired-comparefiles-command "comparefiles")
    (setq auto-mode-alist (cons '("[^/]\\.dired$" . dired-virtual-mode)
                                   auto-mode-alist))
	(dired-x-bind-find-file)
	(setq dired-x-hands-off-my-keys nil)))

(add-hook 'dired-load-hook 'dired-stuff)

(defun dired-up-dir ()
  (interactive
   (find-alternate-file "..")))

(put 'dired-find-alternate-file 'disabled nil)
(define-key dired-mode-map (kbd "RET") 'dired-find-file) ; was dired-advertised-find-file
(define-key dired-mode-map [A-return] 'dired-find-alternate-file) ; was dired-advertised-find-file
(define-key dired-mode-map (kbd "^") 'dired-up-dir)
(define-key dired-mode-map [M-up] 'dired-up-directory)
(define-key dired-mode-map [M-down] 'dired-find-file-other-frame)
(define-key dired-mode-map "\C-cc" 'dired-compare-files)
(define-key dired-mode-map "\C-cf"  'find-dired)
(define-key dired-mode-map "\C-c+" 'dired-create-file)
(define-key dired-mode-map "\C-cx" 'dired-create-executable)
(define-key dired-mode-map "\C-ct" 'mac-open-terminal)
(define-key dired-mode-map "\C-cF" 'mac-show-in-finder)

(setq dired-guess-shell-alist-user
       (list (list "\\.plist\\'" "plutil -convert xml1");; fixed rule
              ;; possibly more rules ...
              (list "\\.bar\'";; rule with condition test
                    '(if condition
                          "BAR-COMMAND-1"
                        "BAR-COMMAND-2"))))

(defun dired-compare-files (file1)
  "Compare file at point with file FILE using `diff'.
FILE defaults to the file at the mark.  (That's the mark set by
\\[set-mark-command], not by Dired's \\[dired-mark] command.)
The prompted-for file is the first file given to `diff'.
With prefix arg, prompt for second argument SWITCHES,
 which is options for `diff'."
  (interactive
   (let ((default (if (mark t)
		      (save-excursion (goto-char (mark t))
				      (dired-get-filename t t)))))
;     (require 'diff)
     (list (read-file-name (format "Compare %s with: %s"
				   (dired-get-filename t)
				   (if default
				       (concat "(default " default ") ")
				     ""))
			   (dired-current-directory) default t))))
  (concat ("comparefiles" file1))
;  (call-process (concat ("comparefiles" file (dired-get-filename t))))
)

(defun dired-find-file-other-frame ()
  (interactive
   (let ((default (if (mark t)
		      (save-excursion (goto-char (mark t))
				      (dired-get-filename t t)))))
  (dired-other-frame default))
))

(defun mac-open-terminal ()
   (interactive)
   (let ((dir ""))
     (cond
      ((and (local-variable-p 'dired-directory) dired-directory)
       (setq dir dired-directory))
      ((stringp (buffer-file-name))
       (setq dir (file-name-directory (buffer-file-name))))
      )
     (do-applescript (format "
tell application \"iTerm2\"
  activate
  try
    do script with command \"cd \" & \"%s\"
  on error
    beep
  end try
end tell" dir)
     )))

(defun mac-open-file ()
  "Document forthcoming..."
  (interactive)
  (let ((file (do-applescript "try
POSIX path of (choose file)
end try")))
    (if (> (length file) 3)
        (setq file
              (replace-regexp-in-string
               "\\\\\\(.\\)" "\\1"
               (decode-coding-string
                (substring file 1 (- (length file) 1))
                'sjis-mac))))
    (if (and (not (equal file ""))(file-readable-p file))
        (find-file file)
      (beep))
    ))

(defun mac-show-in-finder ()
  "Document forthcoming..."
  (interactive)
  (if (stringp (buffer-file-name))
      (do-applescript
       (format "
tell application \"Finder\"
  activate
  try
    select file \"%s\"
  on error
    beep
  end try
end tell"
               (if (eq selection-coding-system 'sjis-mac)
                   (replace-regexp-in-string
                    "\\\\" "\\\\\\\\"
                    (encode-coding-string
                     (posix-file-name-to-mac (buffer-file-name))
                     selection-coding-system))
                 (encode-coding-string
                  (posix-file-name-to-mac (buffer-file-name))
                  selection-coding-system))
               ))
    (shell-command "/usr/bin/open .")
    ))

(defun mac-activate-process (process)
  "Switch to process"
  (interactive "MSwitch to process:")
  (setq aprocess process)
  (do-applescript
   (format "
tell application \"%s\"
  activate
end tell" process)))

(defun dired-create-file (file)
"Add file to current directory"
(interactive "FAdd file named:")
(dired-run-shell-command (concat "touch " file))
(revert-buffer)
)

(defun dired-create-executable (file)
"Create an execuatble file with mode 755"
(interactive "FExecutable file name")
(unless (file-exists-p file)
  (dired-run-shell-command (concat "touch " file)))
(unless (file-executable-p file)
  (dired-run-shell-command (concat "chmod 755 " file)))
(revert-buffer))

(defun dired-compare-files (file1)
  "Compare file at point with file FILE using `diff'.
FILE defaults to the file at the mark.  (That's the mark set by
\\[set-mark-command], not by Dired's \\[dired-mark] command.)
The prompted-for file is the first file given to `diff'.
With prefix arg, prompt for second argument SWITCHES,
 which is options for `diff'."
  (interactive
   (let ((default (if (mark t)
		      (save-excursion (goto-char (mark t))
				      (dired-get-filename t t)))))
;     (require 'diff)
     (list (read-file-name (format "Compare %s with: %s"
				   (dired-get-filename t)
				   (if default
				       (concat "(default " default ") ")
				     ""))
			   (dired-current-directory) default t))))
  (concat ("comparefiles" file1))
;  (call-process (concat ("comparefiles" file (dired-get-filename t))))
)

;;;
;;; Emacs-Lisp
;;;

(add-hook 'emacs-lisp-mode-hook
	  #'(lambda ()
	     (define-key emacs-lisp-mode-map [C-S-right] 'forward-list)
	     (define-key emacs-lisp-mode-map [C-S-left] 'backward-up-list)
	     (define-key emacs-lisp-mode-map [C-right] 'forward-sexp)
	     (define-key emacs-lisp-mode-map [C-left] 'backward-sexp)
	     (define-key emacs-lisp-mode-map [C-up] 'beginning-of-defun)
	     (define-key emacs-lisp-mode-map [C-down] 'end-of-defun)
	     (define-key emacs-lisp-mode-map [C-kp-delete] 'forward-delete-sexp)
	     (define-key emacs-lisp-mode-map [f8] 'describe-foo-at-point)
	     (define-key emacs-lisp-mode-map [A-f8] 'word-at-point-elisp)
	     (define-key emacs-lisp-mode-map [H-S-f8] 'describe-key)
	     (define-key emacs-lisp-mode-map [C-f8] 'find-function-at-point)

	     (define-key emacs-lisp-mode-map [A-S-kp-enter] 'eval-region)
	     (define-key emacs-lisp-mode-map [A-C-S-return] 'eval-region)
	     (define-key emacs-lisp-mode-map [C-enter] 'eval-defun)
	     (define-key emacs-lisp-mode-map [A-C-M-return] 'eval-expression)
	     (define-key emacs-lisp-mode-map [kp-enter] 'eval-last-sexp)
	     (define-key emacs-lisp-mode-map [A-C-return] 'eval-last-sexp)
	     (define-key emacs-lisp-mode-map [M-kp-enter] 'debug-last-sexp)
	     (define-key emacs-lisp-mode-map [M-A-kp-enter] 'edebug-defun)
	     (define-key emacs-lisp-mode-map (kbd "s-c i k") 'insert-last-kbd-macro)
	     (define-key emacs-lisp-mode-map [A-prior] 'case-fold-yes)
	     (define-key emacs-lisp-mode-map [A-next] 'case-fold-no)
         ))

(if (functionp 'global-hi-lock-mode)
	(global-hi-lock-mode 1)
  (hi-lock-mode 1))
(define-key hi-lock-map "\C-z\C-h" 'highlight-lines-matching-regexp)
(define-key hi-lock-map "\C-zi" 'hi-lock-find-patterns)
(define-key hi-lock-map "\C-zh" 'highlight-regexp)
(define-key hi-lock-map "\C-zp" 'highlight-phrase)
(define-key hi-lock-map "\C-zr" 'unhighlight-regexp)
(define-key hi-lock-map "\C-zb" 'hi-lock-write-interactive-patterns)

(defun my-highlight-changes-enable-hook ()
  (add-hook 'local-write-file-hooks 'highlight-changes-rotate-faces)
)

(defun my-highlight-changes-disable-hook ()
  (remove-hook 'local-write-file-hooks 'highlight-changes-rotate-faces)
)

(add-hook 'highlight-changes-enable-hook 'my-highlight-changes-enable-hook)
(add-hook 'highlight-changes-disable-hook
		  'my-highlight-changes-disable-hook)
(add-hook 'forth-mode-hook 'highlight-changes-mode)


;===============
;; macros
(fset  'switch-to-buffers
   "\C-x\C-b\C-xo")
(fset 'my-emacs
   "\C-x\C-f~/.emacs")
(fset 'my-hosts
   [?\C-x ?\C-f ?\C-a ?\C-k ?/ ?s ?u ?d ?o ?: ?r ?o ?o ?t ?@ ?l ?o ?c ?a ?l ?h ?o ?s ?t ?: ?/ ?e ?t ?c ?/ ?h ?o ?s ?t ?s return])
(fset 'my-customizations
   [?\C-x ?\C-f ?~ ?/ ?. ?s ?t ?a ?r ?t ?u ?p ?/ ?. ?e ?m ?a ?c ?s ?. ?d ?/ ?c ?u ?s ?t ?o ?m ?- ?a ?q ?u ?a ?m ?a ?c ?s ?. ?e ?l return])
(fset 'my-gforth.el
   "\C-x\C-f/~/emacs/programmer/gforth.el\C-m\C-x1")
(fset 'my-forth.el
      "\C-x\C-f/~/emacs/programmer/forth.el\C-m\C-x1")
(fset 'my-startup
      "\C-xd/~/startup\C-m\C-x1")
(fset 'my-aliases
  "\C-x\C-f~/startup/01-aliases\C-m\C-x1")
(fset 'my-bash
  "\C-x\C-f~/startup/.bashrc\C-m\C-x1")
; switch over to previous window in buffers
(fset 'toggle-to-previous
   "\C-xb\C-m")
(fset 'truncate-lines-this-buffer
   "\M-'set-variable\C-mtruncate-lines\C-mt\C-m\C-a")
(fset 'dired-gforth
   "\C-xd~/gf\C-m")
(fset 'dired-factor
   "\C-xd~/factor\C-m")
(fset 'dired-skov
   "\C-xd~/skov\C-m")
(fset 'dired-factorwork
   "\C-xd~/factorwork\C-m")
(fset 'dired-skov
   "\C-xd~/factor/extra/skov\C-m")
(fset 'dired-gftools
   "\C-xd~/gftools\C-m")
(fset 'dired-sources
   "\C-xd/~/Sources/\C-m")
(fset 'dired--home
   "\C-xd/~\C-m")
(fset 'kill-this-buffer
   "\C-xk\C-m")
(fset 'switch-to-other-buffer
      "\C-xo\C-x1")
(fset 'only-this-buffer
      "\C-x1")
(fset 'new-buffer
	  "\C-u\C-xb")
(fset 'kill-point-to-end
   [?\M-m kp-end ?\M-x])
(fset 'kill-other-buffer1
   [kp-0 ?\M-w kp-0])
(fset 'kill-other-buffer
   "\C-x4b\C-m")
(fset 'find-blank-line
   [H-return ?s ?e ?a ?r ?c ?h ?- ?f ?o ?r ?w ?a ?r ?d ?- ?r ?e ?g ?e ?x return ?^ ?$ return down])
(fset 'bootrom-with-logger
   "(l 1 DAC if  \" \" logger  then  l)")
(fset 'bootrom-collect-file-info
   [kp-home H-return ?s ?e ?a ?r ?c ?h ?- ?f ?o ?r ?w ?a ?r ?d return ?$ ?I ?d ?: ?  return ?\C-  H-return ?s ?e ?a ?r ?c ?h ?- ?f ?o ?r ?w ?a ?r ?d return ?E ?x ?p ?  ?$ return left left left left left ?\M-c ?\C-x ?\C-j kp-home ?\C-  ?\C-e escape ?\C-w ?\M-c ?\C-x ?b ?* ?t ?e ?x ?t ?- ?s ?c ?r ?a ?t ?c ?h ?* return kp-end return ?\M-v backspace ?/ H-return ?s ?e ?a ?r ?c ?h ?- ?b ?a ?c ?k ?w ?a ?r ?d return ?  return ?\C-  ?\C-a ?\M-x kp-delete ?\C-e ?\M-v ?\C-  ?\C-a ?\M-c])
(fset 'customize-something
   [A-return ?c ?u ?s ?t ?o ?m ?i ?z ?e ?- ?g ?r ?o ?u ?p])
(fset 'customize-what
   [A-return ?c ?u ?s ?t ?o ?m ?i ?z ?e ?- ?a ?p ?r ?o ?p ?o ?s])
; insert the last defined keyboard macro into the buffer (typically here in your .emacs)
(fset 'insert-last-kbd-macro
   [M-return ?n ?a ?m ?e ?- ?l ?a ?s ?t ?- ?k ?b ?d ?- ?m ?a ?c ?r ?o return ?t ?e ?m ?p return M-return ?i ?n ?s ?e ?r ?t ?- ?k ?b ?d ?- ?m ?a ?c ?r ?o return ?t ?e ?m ?p return ?\C-r ?t ?e ?m ?p ?\C-? ?\C-x])
(fset 'other-winodw-and-join
   [ns-application-store-state ?\C-x ?o ?\s-1])
(fset 'find-selection
   [A-left ?\C-s ?\C-w])

(define-key global-map (kbd "A-e") 'find-selection)
(define-key global-map (kbd "M-`") 'next-window-any-frame)
(define-key global-map (kbd "M-~") 'previous-window-any-frame)

;; Unset some keys
(global-unset-key [?\s-h])	;; 'ns-do-hide-emacs
(global-unset-key (kbd "M-`")) ;; tmm menubar

;; Emacs
(global-set-key [A-end] 'cua-mode)
(global-set-key (kbd "M-x") 'clipboard-kill-region)
(global-set-key (kbd "M-c") 'cua-copy-region)
(define-key cua--cua-keys-keymap (kbd "M-v") 'cua-paste)
(global-set-key (kbd "M-z") 'undo)
(global-set-key (kbd "M-s") 'save-buffer)
(global-set-key (kbd "M-w") 'kill-this-buffer)
(global-set-key (kbd "A-M-w") 'delete-frame)
(global-set-key (kbd "M-f") 'isearch-forward-symbol-at-point)
(global-set-key (kbd "H-s") 'isearch-forward)
(global-set-key (kbd "A-e") 'find-selection)
(global-set-key (kbd "M-A-l") 'goto-line)

(define-key global-map (kbd "M-`") 'next-window-any-frame)
(define-key global-map (kbd "M-~") 'previous-window-any-frame)

;; F5 Dired
(global-set-key "\C-x\C-j" 'dired-jump)
(global-set-key [f5] 'dired)
(global-set-key [M-f5] 'vc-dir)
(global-set-key [C-f5] 'magit-status)
(global-set-key [S-f5] 'dired-as-root-local)
(global-set-key [A-f5] 'dired-as-root-pve)
(global-set-key [H-f5] 'dired-as-davec-pve)
(global-set-key [s-f5] 'dired-as-davec-air)
(global-set-key [A-M-f5] 'dired-as-davec-imacm1)
(global-set-key [A-C-f5] 'dired-as-root-onefinity)

;; F6 Search
(global-set-key [M-f6] 'tags-search)
(global-set-key [S-f6] 'grep)
(global-set-key [C-S-f6] 'grep-tree)
(global-set-key [f6] 'search-files)
(global-set-key [H-f6] 'find-grep)

;; F7 Sources
(global-set-key [f7] 'dired-sources)
(global-set-key [S-f7] 'dired-factor)
(global-set-key [A-f7] 'dired-factorwork)
(global-set-key [C-f7] 'dired-skov)

;; F8 At point
(global-set-key [A-M-f8] 'ffap)
(autoload 'dash-at-point "dash-at-point"
  "Search the word at point with Dash." t nil)
(global-set-key [M-f8] 'dash-at-point)


;; F12 buffer switching
(with-system darwin
  (global-set-key [f12] 'bs-show)
  (global-set-key [M-f12] 'my-emacs)
  (global-set-key [C-S-f15] 'my-aliases)
  (global-set-key [A-f12] 'switch-to-buffers)
  (global-set-key [C-f12] 'new-buffer)
  (global-set-key [A-S-f12] 'my-customizations)
  (global-set-key [H-f12] 'my-startup)
  )
(with-system gnu/linux
  (global-set-key [f12] 'bs-show)
  (global-set-key [s-f12] 'my-emacs)
  (global-set-key [C-S-f15] 'my-aliases)
  (global-set-key [M-f12] 'switch-to-buffers)
  (global-set-key [C-f12] 'new-buffer)
  (global-set-key [M-S-f12] 'my-customizations)
)

;; buffer movement
(global-set-key [home] 'beginning-of-buffer)
(global-set-key [C-home] 'beginning-of-buffer-other-window)
(global-set-key [end] 'end-of-buffer)
(global-set-key [C-end] 'end-of-buffer-other-window)
                
(defun switch-window-join ()
  "Switch to other window and join all windows into one window."
  (interactive)
  (other-window 1)
  (delete-other-windows))

(global-set-key [H-1] 'switch-window-join)
(global-set-key [H-2] 'only-this-buffer)

;; arrows
(global-set-key [M-right] 'end-of-line)
(global-set-key [M-left] 'beginning-of-line)
(global-set-key [A-right] 'forward-word)
(global-set-key [A-left] 'backward-word)

;; keypad
;; (global-set-key [kp-numlock] 'global-highlight-changes)
;; (global-set-key [A-kp-numlock] 'highlight-changes-mode)
;; (global-set-key [A-kp-equal] 'show-paren-mode)
;; (global-set-key [A-kp-divide] 'global-hl-line-mode)
;; (global-set-key [A-kp-multiply] 'toggle-truncate-lines)
;; (global-set-key [A-kp-8] 'select-frame-by-name)
;; (global-set-key [A-kp-0] 'other-window)
;; (global-set-key [C-kp-0] 'kill-other-buffer)
;; (global-set-key [kp-subtract] 'bs-cycle-next)
;; (global-set-key [A-kp-subtract] 'other-frame)
;; (global-set-key [C-kp-subtract] 'delete-window)
;; (global-set-key [A-kp-add] 'bs-cycle-previous)
;; (global-set-key [A-M-kp-add] 'new-frame)
;; (global-set-key [A-kp-decimal] 'toggle-to-previous)

;; My stuff
(with-system darwin
     (global-set-key (kbd "M-RET") 'execute-extended-command)
     (global-set-key [A-C-M-return] 'eval-expression)
     (global-set-key [?\s-1] 'delete-other-windows)
     (global-set-key [?\s-2] 'split-window-vertically)
     (global-set-key [?\s-3] 'split-window-vertically)
     (global-set-key [?\s-0] 'tabbar-move-current-buffer-to-new-frame)
     (global-set-key [?\H-c] 'customize-something)
     (global-set-key (kbd "A-/") 'comment-line)
     (global-set-key [s-kp-delete] 'kill-ws-forward)
     (global-set-key [s-backspace] 'kill-ws-backward)
     (global-set-key [A-M-backspace] 'kill-point-to-end)
     ;; (global-set-key (kbd "s-c f x") 'forward-delete-to)
     ;; (global-set-key (kbd "s-c f c") 'forward-copy-to)
     ;; (global-set-key (kbd "s-c i t") 'insert-time)
     ;; (global-set-key (kbd "s-c i d") 'insert-date)
     ;; (global-set-key (kbd "s-c i D") 'insert-short-date)
     ;; (global-set-key (kbd "s-c i T") 'insert-my-tag)
     ;; (global-set-key "\C-c\C-c" 'compile)
     (global-set-key [C-M-next] 'find-word-at-point-forward)
     (global-set-key [C-M-prior] 'find-word-at-point-backward)
     )
(with-system gnu/linux
  (global-set-key [M-return] 'execute-extended-command)
  (global-set-key [?\H-z] 'undo)
  (global-set-key [?\H-x] 'kill-region)
  (global-set-key [?\C-c] 'copy-region-as-kill)
  (global-set-key [?\C-v] 'yank)
  (global-set-key [?\s-w] 'kill-this-buffer)
  (global-set-key [?\s-/] 'comment-or-uncomment-region)
  (global-set-key [?\s-s] 'save-buffer)
  )

;; Bookmarks
(global-set-key [C-w] 'word-at-point-copy)
(global-set-key [f11] 'list-bookmarks)

(global-set-key [C-f11] 'bookmark-word-at-point)
(global-set-key [A-M-f11] 'bookmark-delete-word-at-point)
(global-set-key [C-Mf11] 'bookmark-jump)

;; Mouse events

;===============
;; Functions
(defun dired-as-root-local (path)
  "Dired as root onto the localhost to given PATH."
  (interactive "DDired as root to:")
  (if (equal (substring (system-name) -1) ".")
	  (setq my-local-sysname (substring (system-name) 0 (- (length (system-name)) 1)))
	(setq my-local-sysname (system-name)))
  (dired (concat "/sudo::" path)))

(defun dired-as-root-pve (path)
  "Dired as root onto pve.local to given PATH."
  (interactive "sDired as root to 10.1.1.1:")
  (dired (concat "/ssh:root@10.1.1.1:" path)))

(defun dired-as-root-sl (path)
  "Dired as root onto pve.local to given PATH."
  (interactive "sDired as root to sl.polymicro.net:")
  (dired (concat "/ssh:root@sl.polymicro.net:" path)))

(defun dired-as-davec-pve (path)
  "Dired as davec onto pve.local to given PATH."
  (interactive "sDired as davec to pve.local:")
  (dired (concat "/ssh:davec@pve.local:" path)))

(defun dired-as-tgunr-crs (path)
  "Dired as root onto corneredrats.com to given PATH."
  (interactive "sDired as tgunr to kingkong.corneredrats.com:")
  (dired (concat "/tgunr@kingkong.corneredrats.com:" path)))

(defun dired-as-davec-air (path)
  (interactive "sDired as davec to air.local:")
  (dired (concat "/ssh:davec@air.local:" path)))

(defun dired-as-davec-imacm1 (path)
  (interactive "sDired as davec to imacm1.local:")
  (dired (concat "/ssh:davec@imacm1.local:" path)))

(defun dired-as-root-onefinity (path)
  (interactive "sDired as root to onefinity.local:")
  (dired (concat "/ssh:root@onefinity.local:" path)))

(defun dired-as-davec-braeburn (path)
  "Dired as davec onto BRAEBURN.PSY.CMU.EDU to given PATH."
  (interactive "DDired as davec to braeburn:")
  (dired (concat "/davec@BRAEBURN.PSY.CMU.EDU:" path)))

(defun dired-as-root-tv (path)
  "Dired as root onto appletv.pmnet to given PATH."
  (interactive "DDired as root to root@appletv.pmnet:")
  (dired (concat "/root@appletv.pmnet:" path)))

(defun dired-pc (path)
  "Dired as davec to given PATH."
  (interactive "DDired as davec to host:")
  (dired (concat "/davec@10.1.1.13:" path)))

(defun dired-crs (server)
  "Dired as root onto server at corneredrats.com to /."
  (interactive "sDired as root to: ")
  (let ((ending ".crs:/"))
    (if (string-match "[1-9]+" server)
        (setq ending ":/"))
    (dired (concat "/ssh:root@" server ending))))

(defun current-word-bounds (&optional strict really-word)
  "Return the symbol or word that point is on (or a nearby one) as a string.
The return value includes no text properties.
If optional arg STRICT is non-nil, return nil unless point is within
or adjacent to a symbol or word.  In all cases the value can be nil
if there is no word nearby.
The function, belying its name, normally finds a symbol.
If optional arg REALLY-WORD is non-nil, it finds just a word."
  (save-excursion
    (let* (
	   (oldpoint (point))
	   (start (point))
	   (end (point))
	   (syntaxes (if really-word "w" "w_"))
	   (not-syntaxes (concat "^" syntaxes)))
      (skip-syntax-backward syntaxes) (setq start (point))
      (goto-char oldpoint)
      (skip-syntax-forward syntaxes) (setq end (point))
      (when (and (eq start oldpoint) (eq end oldpoint)
		 ;; Point is neither within nor adjacent to a word.
		 (not strict))
	;; Look for preceding word in same line.
	(skip-syntax-backward not-syntaxes
			      (save-excursion (beginning-of-line)
					      (point)))
	(if (bolp)
	    ;; No preceding word in same line.
	    ;; Look for following word in same line.
	    (progn
	      (skip-syntax-forward not-syntaxes
				   (save-excursion (end-of-line)
						   (point)))
	      (setq start (point))
	      (skip-syntax-forward syntaxes)
	      (setq end (point)))
	  (setq end (point))
	  (skip-syntax-backward syntaxes)
	  (setq start (point))))
      ;; If we found something nonempty, return it as a string.
      (unless (= start end)
	(list start end)))))

(defun my-word-at-point ()
  ;; sigh, function-at-point is too clever.  we want only the first half.
  (cond ((setq sym (ignore-errors
		     (save-excursion
		       (or (not (zerop (skip-syntax-backward "_w")))
			   (eq (char-syntax (char-after (point))) ?w)
			   (eq (char-syntax (char-after (point))) ?_)
			   (forward-sexp -1))
		       (skip-chars-forward "`'")
		       (let ((obj (read (current-buffer))))
			 (and (symbolp obj) (fboundp obj) obj))
		       )))
	(symbol-name sym))))

(defun word-at-point-copy ()
  "Copies the word under the point so you can paste it in elsewhere."
  (interactive)
  (let ((bounds (current-word-bounds)))
  (clipboard-kill-ring-save (pop bounds) (pop bounds))))

(defun bookmark-word-at-point ()
  "Enter the word under the point as a bookmark."
  (interactive)
  (bookmark-set (current-word))
  (message (concat "Bookmarked: " (current-word))))

(defun bookmark-delete-word-at-point ()
  "Enter the word under the point as a bookmark."
  (interactive)
  (bookmark-delete (current-word))
  (message (concat "Bookmark Deleted: " (current-word))))

(defun find-word-at-point-backward (count)
  "Locate the word at the point backward in the buffer.
With an argument, will skip COUNT occurances."
    (interactive "P")
	(push-mark)
	(let ((word (my-word-at-point)))
	  (condition-case err
		  (search-backward word (point-min) nil count)
		(search-failed
		 (message "Can't find %s" word)))))

(defun find-word-at-point-forward (count)
  "Locate the word at the point forward in the buffer.
With an argument, will skip COUNT occurances."
    (interactive "P")
	(push-mark)
	(let ((word (my-word-at-point)))
	  (goto-char (+ (point) (length word)))								   ; move past the word
	  (condition-case err
		  (search-forward word (point-max) nil count)
		(search-failed
		 (message "Can't find %s" word)))
	  (goto-char (- (point) (length word)))))							   ; back up so point is at start

(defun describe-foo-at-point ()
  "Show the documentation of the Elisp function and variable near point.
This checks in turn:

-- for a function name where point is
-- for a variable name where point is
-- for a surrounding function call
"
  (interactive)
  (let (sym)
    ;; sigh, function-at-point is too clever.  we want only the first half.
    (cond ((setq sym (ignore-errors
                       (with-syntax-table emacs-lisp-mode-syntax-table
                         (save-excursion
                           (or (not (zerop (skip-syntax-backward "_w")))
                               (eq (char-syntax (char-after (point))) ?w)
                               (eq (char-syntax (char-after (point))) ?_)
                               (forward-sexp -1))
                           (skip-chars-forward "`'")
                           (let ((obj (read (current-buffer))))
                             (and (symbolp obj) (fboundp obj) obj))))))
           (describe-function sym))
          ((setq sym (variable-at-point)) (describe-variable sym))
          ;; now let it operate fully -- i.e. also check the
          ;; surrounding sexp for a function call.
          ((setq sym (function-at-point)) (describe-function sym)))))

(defun forward-delete-sexp (&optional count)
  "Deletes forward across balanced expression (sexp).
With a count, does it that many times. Negative count means
move backward across N balanced expressions"
  (interactive)
  (save-excursion
    (forward-sexp-mark count)
    (clipboard-kill-region (point) (mark))))

(defun kill-point-to-end ()
  "Delete from current point to end of buffer"
  (interactive)
  (save-excursion
	(let ((here (point))
		  (there (point-max)))
		  (clipboard-kill-region here there))))

(defun forward-delete-to (regexp)
  "Delete forward to given regexp, like vi"
  (interactive "MTo:")
  (save-excursion
	(let ((here (point))
		  (there (re-search-forward regexp)))
		  (clipboard-kill-region here there)))) 


(defun kill-ws-forward ()
  "Kill from point to next character spanning all whitespace and newlines"
  (interactive)
  (save-excursion
    (delete-region (point)
		   (+ (save-excursion (skip-chars-forward " \t\n")) (point))
		   )))

(defun kill-ws-backward ()
  "Kill from point to previous character spanning all whitespace and newlines"
  (interactive)
  (save-excursion
    (let ((there (point))
	  (here ((skip-chars-backward " \t\n") (point)))
    (delete-region here there)))))

(defun forward-copy-to (regexp)
  "Delete forward to given regexp, like vi"
  (interactive "MTo:")
  (save-excursion
	(let ((here (point))
		  (there (- (re-search-forward regexp) (length regexp))))
		  (clipboard-kill-ring-save here there))))

(defun word-at-point-elisp (count)
  "Bring up the info for the word at the point. Will skip n words if COUNT is given."
  (interactive "p")
  (push-mark)
  (let ((word (current-word)))
    (info "(elisp)")
    (Info-index word)))

(defcustom dns-assignment-column 32
  "Align assignments to this column by default with \\[dns-align-assignments].
If this number is negative, the `=' comes before the whitespace.  Use 0 to
not align (only setting space according to `dns-assignment-space')."
  :type 'integer
  :group 'conf)

(defun dns-align-assignments (&optional arg)
  (interactive "P")
  "Align the assignments in the buffer or active region.
In Transient Mark mode, if the mark is active, operate on the
contents of the region.  Otherwise, operate on the whole buffer."
  (setq arg (if arg
		(prefix-numeric-value arg)
	      dns-assignment-column))
  (save-excursion
    (save-restriction
      (when (use-region-p)
	(narrow-to-region (region-beginning) (region-end)))
      (goto-char (point-min))
      (while (not (eobp))
	(let ((cs (comment-beginning)))	; go before comment if within
	  (if cs (goto-char cs)))
	(while (forward-comment 9))	; max-int?
	(when (and (not (eobp)))
          (progn
           (if   (looking-at ".+?\\([ \t]+A[ \t]+\\)")
               (progn
                 (goto-char (match-beginning 1))
                 (delete-region (point) (match-end 1))
                 (indent-to-column arg)
                 (insert ?A?\t))
             (if (looking-at ".+?\\([ \t]+CNAME[ \t]+\\)")
                 (progn
                   (goto-char (match-beginning 1))
                   (delete-region (point) (match-end 1))
                   (indent-to-column arg)
                   (insert "CNAME" ?\t))
               (if   (looking-at ".+?\\([ \t]+IN[ \t]+\\)")
                   (progn
                     (goto-char (match-beginning 1))
                     (delete-region (point) (match-end 1))
                     (indent-to-column arg)
                     (insert "IN"?\s))
                 (if   (looking-at "\\([ \t]+NS[ \t]+\\)")
                     (progn
                       (goto-char (match-beginning 1))
                       (delete-region (point) (match-end 1))
                       (indent-to-column arg)
                       (insert "NS"?\t))
               ))))
           (forward-line)
           )
          )
        )
      )
    )
  )


(defun dired-mouse-find-file-this-window (event)
  "In Dired, visit the file or directory name you click on in another frame.
Argument EVENT mouse."
  (interactive "e")
  (let (window pos file)
    (save-excursion
      (setq window (posn-window (event-end event))
	    pos (posn-point (event-end event)))
      (if (not (windowp window))
	  (error "No file chosen"))
      (set-buffer (window-buffer window))
      (goto-char pos)
      (setq file (dired-get-file-for-visit)))
    (select-window window)
    (find-file-other-frame (file-name-sans-versions file t))))

(defcustom insert-time-format "%X"
  "**Format for \\[insert-time] (c.f. 'format-time-string').")

(defcustom insert-date-format "%x"
  "**Format for \\[insert-date] (c.f. 'format-date-string').")

(defcustom insert-short-date-format "%a %b %d, %Y"
  "**Format for \\[insert-date] (c.f. 'format-date-string').")

(defun insert-my-tag ()
  "Insert a tag with my intials and a date stamp."
  (interactive "*")
  (insert (format-time-string "DAC-%x")))

(defun insert-ofstamp ()
  "Insert an updated stamp into OF source code."
  (interactive "*")
  (insert (format-time-string "%x %X")))

(defun insert-time ()
  "Insert the current time."
  (interactive "*")
  (insert (format-time-string insert-time-format)))

(defun insert-date ()
  "Insert the current date."
  (interactive "*")
  (insert (format-time-string insert-date-format)))

(defun insert-short-date ()
  "Insert the current date in short format mmm dd yy dow."
  (interactive "*")
  (insert (format-time-string insert-short-date-format)))

(setq write-file-functions (list 'bootrom-update-ofstamps))

(defun bootrom-update-ofstamps ()
  "Find a writestamp and update it with the current time. A ofstamp looks like: .( UPDATED: 10/24/02 18:44:58)."
  (if (equal mode-name "Forth")
  (save-excursion
	(save-restriction
	  (save-match-data
		(widen)
		(goto-char (point-min))
		(while (search-forward ".( UPDATED: " nil t)
		  (let ((start (point)))
			(search-forward ")")
			(delete-region start (- (point) 1))
			(goto-char start)
			(insert-ofstamp)))))))
  nil)

(defun bootrom-collect-checkin ()
  "Collect the file path and revision for release notes"
  (interactive "*")
  (if (equal mode-name "Forth")
	  (save-excursion
		(save-restriction
		  (save-match-data
			(widen)
			(let ((beg (goto-line 2)))
				  (copy-region-as-kill (point-min) (beg)))
			))
nil)))

(add-hook 'log-edit-done-hook 'bootrom-add-cvs-tag)

(defun save-cvs-message-file()
  "Grab the commit message and save it to a temp file.  Should return the temp
file when complete..."

  (save-excursion
    (let (message (temp-file temp-buffer))

      (set-buffer temp-buffer)
	  (find-file (log-edit-initial-files))

      (setq message (buffer-substring-no-properties (point-min) (point-max)))

      (setq temp-file (make-temp-name (concat (getenv "TEMP") "/cvs-commit-directory-")))

      (setq temp-buffer (find-file-noselect temp-file))

      (set-buffer temp-buffer)

      (insert message)

      (save-buffer)

      (kill-buffer temp-buffer)

      ;;return the buffer name we are using...
      (buffer-file-name temp-buffer))))

(defun bootrom-add-cvs-tag ()
  "Add a MPW style comment to the header when CVS checkin comment is entered."
  (interactive "*")
  (if (equal mode-name "Forth")
	  (let ((saved-message (save-cvs-message-file)))
		(find-file 'log-edit-initial-files)
		nil )
	))

(defun create-tags (dir-name)
     "Create tags file."
     (interactive "DDirectory: ")
     (eshell-command
      (format "find %s -type f -name \"*.[ch]\" | etags -" dir-name)))

(defadvice find-tag (before c-tag-file activate)
   "Automatically create tags file."
   (let ((tag-file (concat default-directory "TAGS")))
     (unless (file-exists-p tag-file)
       (shell-command "etags *.[ch] -o TAGS 2>/dev/null"))
     (visit-tags-table tag-file)))

(global-set-key [?\C-/] 'complete-tag)

(defadvice switch-to-buffer (before existing-buffer activate compile)
  "When interactive, switch to existing buffers only, unless given a prefix argument."
  (interactive (list
				(read-buffer "Switch to buffer: "
							 (other-buffer)
							 (null current-prefix-arg)))))

(defun current-frame ()
  (frame-parameter nil 'buffer-list)
)

(defun ediff-top-buffers ()
  "Perform an ediff-buffers on the most recent two buffers."
  (interactive "*")
  (let ((bufs (current-frame)))
        (ediff-buffers (pop bufs) (nth 1 bufs))
    ))

(defun set-frame-mono ()
  (interactive "*")
  (set-frame-font "-apple-profont-medium-r-normal--12-90-75-75-m-90-mac-roman" t)
)

(defun set-frame-serif ()
  (interactive "*")
  (set-frame-font "-apple-optima-medium-r-normal--0-0-75-75-m-0-mac-roman" t)
)

;===============
; My functions

(defun buffer-current-line (&optional buffer)
  "Return the content of the current line in the buffer.
If optional buffer specified, then return its current line contents"
  (if buffer (with-current-buffer buffer
	       (buffer-substring (line-beginning-position) (line-end-position)))
    (buffer-substring (line-beginning-position) (line-end-position))))

(defun chronosync-errors (buffer &optional starting-line)
  "Parse a buffer containing lines from a Chronosync log and convert it to a buffer of pathnames"
  (interactive "bBuffer: ")
  (if buffer
      (save-current-buffer
	(set-buffer buffer)
	(when (interactive-p)
	    (setq starting-line (read-number "Line: " 1)))
	(goto-line starting-line)
	(let ( (start nil) )
	  (while (not (eobp))
	    (if (not (looking-at "^.[0-9 :-]+] +\\(/.*\\)"))
		(kill-line)
	      (setq start (line-beginning-position))
	      (delete-region start (match-beginning 1))
	      (beginning-of-line)
	      (insert "'")
	      (end-of-line)
	      (insert "'")
	      (forward-line)))))))

(defun my-parse-crashlog (buffer &optional starting-line)
  "Parse a crash log report"
   (interactive "bBuffer: ")
  (if buffer
      (save-current-buffer
	(set-buffer buffer)
	(goto-line 1)
	(let ( (start nil) (regValue nil))
	   (re-search-forward "^Thread [0-9]+ crashed with")
	   (search-forward "eax: ")
	   (setq start (point))
	   (search-forward " ")
	   (setq regEAX (buffer-substring-no-properties (+ start 2) (point)))
	   (setq regEAX (string-to-number regValue 16))
	   (search-forward "ebx: ")
	   (setq start (point))
	   (search-forward " ")
	   (setq regEBX (buffer-substring-no-properties (+ start 2) (point)))
	   (setq regEBX (string-to-number regValue 16))
	   (search-forward "ebx: ")
	   (setq start (point))
	   (search-forward " ")
	   (setq regEBX (buffer-substring-no-properties (+ start 2) (point)))
	   (setq regEBX (string-to-number regValue 16))

))))

(defun between (num min max) (and (> num min) (< num max)))
(defun within (num min max)
  (unless (eq num nil)
    (and (>= num min) (<= num max))))
(defun indexof (value list)
  (catch 'loop
    (let ((index nil)(i 0))
      (dolist (aValue list)
	(if (equal aValue value)
	    (throw 'loop i))
	(setq i (1+ i))))
))


(defun parse-crashlog (fileName &optional starting-line)
  "Parse a crash log report"
  (interactive "fFile: ")
  (setq crashlogfile (find-file-noselect fileName))
  (if crashlogfile
      (save-current-buffer
	(set-buffer crashlogfile)
	(goto-line 1)
	(unless (eq starting-line nil)
	  (goto-line starting-line))

	(let ( (start nil) (regValue nil)
	       opoint beg end value regName regNames codeType codeTypes
	       (typeX86-64 "X86-64 (Native)")
	       (typeX86 "X86 (Native)")
	       (typePPCT  "PPC (Translated)")
	       )
	  (re-search-forward "^Code Type:\s+")
	  (setq opoint (point))
	  (end-of-line)
	  (setq codeType (buffer-substring-no-properties opoint (point)))
	  (setq codeTypes (list typeX86-64 typeX86 typePPCT))
	  (if (not (eq nil (member codeType codeTypes)))
	      (progn
		(cond (
		       (equal codeType typeX86-64)
		       (setq regNames (list "rax" "rbx" "rcx" "rdx" "rdi" "rsi" "rbp" "rsp"
					     "r8" "r9" "r10" "r11" "r12" "r13" "r14" "r15"
					     "rip" "rfl")))
		       (
			(or (equal codeType typePPCT) (equal codeType typeX86))
			(setq regNames (list "eax" "ebx" "ecx" "edx" "edi" "esi" "ebp" "esp")))
		       (t (message "Unkown type"))
		       )
		(re-search-forward "^Thread [0-9]+ crashed with")
		(setq regValues (list))
		(setq regHexValues (list))
		(dolist (aRegName regNames)
		  (progn
		    (setq regName (concat aRegName ": "))
		    (search-forward regName)
		    (setq bounds (current-word-bounds))
		    (setq regValue (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
		    (setq regHexValues (cons regValue regHexValues))
		    (setq regValue (string-to-number regValue 16))
		    (setq regValues (cons regValue regValues))))
		(setq regValues (reverse regValues))
		(setq regHexValues (reverse regHexValues))
		(search-forward "Binary Images:")
		(re-search-forward "^")
		(setq opoint (point))
		(forward-page)
		(beginning-of-line)
		(or (looking-at page-delimiter)
		    (end-of-line))
		(setq end (point))
		(goto-char opoint)
		(setq beg (point))
		(setq linecount (count-lines beg end))
		;; (setq reglist (list regEAX regEBX regECX regEDX regEDI regESI regEBP regESP))
		(while (not (eq linecount 0))
		  (if (looking-at "^\s*0x")
		      (progn
			(forward-word)
			(setq bounds (current-word-bounds))
			(setq loRange (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
			(setq loRange (string-to-number loRange 16))
			(forward-word)
			(setq bounds (current-word-bounds))
			(setq hiRange (buffer-substring-no-properties ( + (pop bounds) 2) (pop bounds) ))
			(setq hiRange (string-to-number hiRange 16))
			(re-search-forward " /")
			(goto-char (1- (point)))
			(setq foundHome (looking-at ".*davec"))
			(setq foundReg nil)
			(dolist (regValue regValues)
			  (if (within regValue loRange hiRange)
			      (progn
				(setq foundReg t)
				(setq foundRegName (nth (indexof regValue regValues) regNames))
				(setq foundRegHexValue (nth (indexof regValue regValues) regHexValues))
			    )
			    ))
			(if (and (eq foundReg nil) (eq foundHome nil))
			    (progn
			      (beginning-of-line)
		  	      (insert "#REGNONE ")
			      )
			  (if (eq foundReg nil)
			      (progn
				(beginning-of-line)
				(insert     "#REGNONE ")
				)
			    (beginning-of-line)
			    (insert (concat "#REG: " foundRegName))
			    )
			  )
			)
		    (forward-line)
		    (setq linecount (1- linecount))
		    )
		  )
		)
	    (message "File is not recognized as a known architecture")
	    )))))

(defun mac2unix ()
  (interactive)
  (save-excursion
	(goto-char (point-min))
	(while (search-forward "\r" nil t) (replace-match "\n"))))

(require 'grep)

(defun search-files (command-args)
  "Perform 'search` command and switch to its result buffer."
  (interactive
   (progn
	 (let ((default "/opt/homebrew/bin/ag --nocolor " ))
       (list (read-from-minibuffer "Run search (like this): "
								   default
;								   (if current-prefix-arg default default )
								   nil nil 'grep-history default
;								   (if current-prefix-arg nil default)
)))))

  ;; Setting process-setup-function makes exit-message-function work
  ;; even when async processes aren't supported.
  (let (
        (compilation-process-setup-function 'grep-process-setup)
        (buf (compilation-start
              (if
                  (concat command-args " " null-device)
                   command-args)))))
              ;; (other-window 1)
              ;;  (toggle-truncate-lines t)
              )

(defun my-grep (command-args &optional highlight-regexp)
  "Run grep, with user-specified args, and collect output in a buffer.
While grep runs asynchronously, you can use \\[next-error] (M-x next-error),
or \\<grep-minor-mode-map>\\[compile-goto-error] in the grep \
output buffer, to go to the lines
where grep found matches.

This command uses a special history list for its COMMAND-ARGS, so you can
easily repeat a grep command.

A prefix argument says to default the argument based upon the current
tag the cursor is over, substituting it into the last grep command
in the grep command history (or into `grep-command'
if that history list is empty).

If specified, optional second arg HIGHLIGHT-REGEXP is the regexp to
temporarily highlight in visited source lines."
  (interactive
   (progn
     (let ((cmd "~/bin/searchf/search -ni "))
       (list (read-from-minibuffer "Run search (like this): " cmd nil nil 'grep-history
   )))))

  ;; Setting process-setup-function makes exit-message-function work
  ;; even when async processes aren't supported.
  (let ((compilation-process-setup-function 'grep-process-setup))
  (compilation-start command-args 'grep-mode nil highlight-regexp))
  (other-window 1)
  (toggle-truncate-lines t))
         
(defun xcode-break-lines ()
"Break up long lines from xcode"
(interactive
 (progn
   (save-excursion
     (while (re-search-forward " \\(-\\([-=./a-zA-Z0-9]+\\)\\)" (point-max) t)
       (replace-match "\n\\1")))
     (save-excursion
       (while (re-search-forward  "\\\"\\(-\\(?:[-=./ a-zA-Z0-9]+\\)\\)\\\"" (point-max) t)
	 (replace-match "\n\\1")))
   )))

(defun xcode-join-lines ()
  "Join lines broken by xcode-break-lines"
  (interactive
   (progn
	 (save-excursion
	   (goto-char (point-min))
	   (while (re-search-forward "\\(\n\\)" (point-max) t)
		 (replace-match " "))))))

(defun forth-grep (command-args)
  "Perform 'find` command on BootROM sources and switch to its result buffer."
  (interactive
   (progn
	 (let ((default "find ~/br/ -path '*/c3' -prune -o -path '*CVS' -prune -o  -type f \\( -name '*.of' -o -name '*.fth' -o -name '*.fas' -o -name '*.s' -o -name '*.fs' -o -name '*.fo' -o -name '*.4th' -o -name '*.msp' -o -name '*.f' \\)  -exec grep -i -nH {} \\;" ))
       (list
	(read-from-minibuffer "Run search (like this): " default  nil nil 'grep-history default)
	)
       )
	 )
   )

  ;; Setting process-setup-function makes exit-message-function work
  ;; even when async processes aren't supported.
  (let*
      (
       (compilation-process-setup-function 'grep-process-setup)
       (buf
	(compile-internal
	 (if
					;(and grep-use-null-device null-device)
	     (concat command-args " " null-device)
	     command-args)
	 "No more search hits" "grep"
	 ;; Give it a simpler regexp to match.
	 nil grep-regexp-alist)
	)
       )
    )
  (other-window 1)
  (toggle-truncate-lines t)
  )

; Avoid superflous lines in make -d output
(defvar my-make-debug-command "make -w -d|grep -v -E ")
(defvar my-make-debug-pattern "No implicit|Trying implicit|Trying pattern|Looking for a|Avoiding implicit|Rejecting impossible")
(defvar my-make-debug-normal "make -w")

(defun my-make-debug ()
  "Set compile-command to my debug version"
  (interactive)
  (set (make-local-variable 'compile-command)
	   (concat my-make-debug-command "\"" my-make-debug-pattern "\"")))

(defun my-make-normal ()
  "Set compile-command back to previous value"
  (interactive)
  (set (make-local-variable 'compile-command) my-make-debug-normal))

(defun debug-last-sexp ()
  "Evaluate and instrument last sexp then execute it."
  (interactive)
  (let (
	(func (eval-last-sexp nil)))
    (edebug-defun)
    (command-execute func)))

(defun instrument-last-sexp ()
  "Instrument last sexp for debugging"
  (edebug-defun))

;;;
; GUD Stuff
;;;
;; (require 'gud)

;; (defun gud-buffer ()
;;   "Return the first buffer found beginning with the name gud-"
;;   (interactive)
;;   (let (
;; 	(abuf)
;; 	(gud-buffer)
;; 	(buflist (buffer-list)))
;;     (while buflist
;;       (setq abuf (car buflist))
;;       (if (string-match "*gud-*" (buffer-name abuf))
;; 	 (progn (setq gud-buffer abuf)
;; 		(setq buflist nil))
;; 	(setq buflist (cdr buflist))))
;;     gud-buffer))

;; (defun address-at-point-forward (count)
;;   "Locate the address at the point forward in the buffer.
;; With an argument, will skip COUNT occurances."
;;     (interactive "i")
;; 	(push-mark)
;; 	(let ((word (my-word-at-point))
;; 	      (reg "")
;; 	      (addr ""))
;; 	  (if (looking-at "%\\(.*\\),.*")
;; 	      (setq word (match-string 1)
;; 		    (use-buffer (gud-buffer))
;; ))))

;; (defun gud-object-at-point ()
;;   "Display the object at the address under the point"
;;   (interactive)
;;   (let ((address (current-word))
;; 	(gudbuf (gud-buffer)))
;;     (with-current-buffer gudbuf
;;       (end-of-buffer)
;;       (insert (concat "po " address))
;;       (comint-send-input)
;;       (switch-to-buffer gudbuf))))

;; (defun gud-break-at-point ()
;;   (interactive)
;;   (let (
;; 	(gudbuf (gud-buffer))
;; 	(spot)
;; 	(startpoint (point))
;; 	)
;; 	(with-current-buffer gudbuf
;; 	  (if (re-search-forward "[:word:]")
;; 		 (progn
;; 		   (if (re-search-backward "[\t ]+" (line-beginning-position) t)
;; 		       (goto-char (+ (point) 1)))
;; 		   (set-mark (point))
;; 		   (end-of-line)
;; 		   (setq spot (buffer-substring-no-properties (mark) (point)))
;; 		   )
;; 	    (progn
;; 	      (goto-char startpoint)
;; 	  (if (or (looking-at "0x[0-9]+") (looking-at "#[0-9]+"))
;; 	      (if (re-search-forward "[+-]\\[" (line-end-position) t)
;; 		  (progn
;; 		    (set-mark (- (point) 2))
;; 		    (re-search-forward "]")
;; 			(setq spot (buffer-substring-no-properties (mark) (point))))))))
;; 	  (if spot (progn
;; 		     (deactivate-mark)
;; 		     (end-of-buffer)
;; 		     (insert (concat "br " spot))))
;; )))

;(defadvice gud-basic-call (after goto-comint-buffer-end activate)
;  "Go to end of buffer after making call"
;  (end-of-buffer))
;; (defun gud-load-fscript ()
;;   "Create strings to load FScript into gdb"
;;   (end-of-buffer)
;;   (insert "p (char)[[NSBundle bundleWithPath:@\"/Library/Frameworks/FScript.framework\"] load]\n"))

;; (gud-def gud-po "po %a" "\C-o" "Print object at address at point.")
;; (gud-def gud-bt "tbreak * %a" "\C-T" "Temporary break at address at point")
;; (gud-def gud-dm "dm %a 100" "d" "Dump memory at address at point")
;; (gud-def gud-bratpoint (gud-break-at-point) "b" "Breakpoint at point with address")
;; (gud-def gud-fscript-load "p (char)[[NSBundle bundleWithPath:@\"/Library/Frameworks/FScript.framework\"] load]\n" "\C-f" "Load FScript menu")
;; (gud-def gud-fscript-menu "p (void)[FScriptMenuItem insertInMainMenu]" "\C-m" "Load FScript menu")

;; (define-key gud-mode-map [M-down-mouse-1] 'gud-dm)
;; (define-key gud-mode-map [M-A-mouse-1] 'gud-po)
;; (define-key gud-mode-map [A-r] 'gud-break-at-point)

;;;
; Shell stuff
;;;

;; (defun my-shell ()
;;   (interactive)
;;   (switch-to-buffer "*shell*")
;;   (shell))

;; (add-hook 'shell-load-hook
;; 		  (lambda ()
;; 			(define-key shell-mode-map [up] 'comint-previous-prompt)
;; 			(define-key shell-mode-map [down] 'comint-next-prompt)
;; 			))

(if (string-match "Aquamacs" (car (split-string (emacs-version) "\n")))
(with-system darwin
     (defvar my-toggle-ofob-setting nil)
     (defun my-toggle-ofob ()
       "Toggles the one-frame per buffer setting. Presumes the default setting on entry is the default value non-nil"
       (interactive)
       (if (eq my-toggle-ofob-setting t)
	   (progn
	     '(one-buffer-one-frame nil nil (aquamacs-frame-setup))
	     (setq my-toggle-ofob-setting nil)
	     (print "true"))
	 '(one-buffer-one-frame t nil (aquamacs-frame-setup))
	 (setq my-toggle-ofob-setting t)
	 (print "false")
	 )))
) 

;; (defadvice dired-advertised-find-file (around dired-subst-directory activate)
;;   "Replace current buffer if file is a directory."
;;   (interactive)
;;   (let ((orig (current-buffer))
;;         (filename (dired-get-filename)))
;;     ad-do-it
;;     (when (and (file-directory-p filename)
;;                (not (eq (current-buffer) orig)))
;;       (kill-buffer orig))))

;; (if (functionp 'global-hi-lock-mode) (global-hi-lock-mode 1) (hi-lock-mode 1))
;; (define-key hi-lock-map "\C-z\C-h" 'highlight-lines-matching-regexp)
;; (define-key hi-lock-map "\C-zi" 'hi-lock-find-patterns)
;; (define-key hi-lock-map "\C-zh" 'highlight-regexp)
;; (define-key hi-lock-map "\C-zp" 'highlight-phrase)
;; (define-key hi-lock-map "\C-zr" 'unhighlight-regexp)
;; (define-key hi-lock-map "\C-zb" 'hi-lock-write-interactive-patterns)

;; (defun my-highlight-changes-enable-hook ()
;;   (add-hook 'local-write-file-hooks 'highlight-changes-rotate-faces)
;; )

;; (defun my-highlight-changes-disable-hook ()
;;   (remove-hook 'local-write-file-hooks 'highlight-changes-rotate-faces)
;; )

;; (add-hook 'highlight-changes-enable-hook 'my-highlight-changes-enable-hook)
;; (add-hook 'highlight-changes-disable-hook
;; 		  'my-highlight-changes-disable-hook)

;; (add-hook 'forth-mode-hook 'highlight-changes-mode)

;===============
;; load paths
(add-to-list 'load-path "~/emacs/bin")
(add-to-list 'load-path "~/emacs/contributed")
;; (add-to-list 'load-path "~/emacs/programmer")
(add-to-list 'load-path "~/emacs/packages")

;; ;;; Contributed sources
;; (require 'bigint)
;; (require 'generic)
;; (require 're-builder+)
;; (defun reb-query-replace (to-string)
;;       "Replace current RE from point with `query-replace-regexp'."
;;       (interactive
;;        (progn (barf-if-buffer-read-only)
;;               (list (query-replace-read-to (reb-target-binding reb-regexp)
;;                                            "Query replace"  t))))
;; 		(reb-quit)
;; 		(switch-to-buffer reb-target-buffer)
;;         (query-replace-regexp (reb-target-binding reb-regexp) to-string))
;; (define-key reb-mode-map "\C-cr" 'reb-query-replace)

;;(require 'c-includes)
;;(require 'cascade-frame)
;; (require 'colours)
;; (require 'tabbar)
;; (require 'google)
;; (require 'swbuff)
;; (require 'xml-parse)
;; (require 'bubble)
;; (global-set-key [kp-add] 'bubble-buffer)
;; (global-set-key [kp-subtract] 'bubble-buffer-back)
;; (require 'osx-plist)

;; Flashing Parens
;; (require 'flash-paren)
;; (flash-paren-mode t)

(require 'findr)
;(require 'consult-dir)


;;; Forth
;; (require 'gforth)
;; (require 'forth)
;; (autoload 'forth-mode "forth.el")
;; (setq auto-mode-alist (cons '("\\.f\\'" . forth-mode) auto-mode-alist))

;; (autoload 'forth-mode "forth-mode.el")
;; (setq auto-mode-alist (cons '("\\.fs\\'" . forth-mode)
;; 							auto-mode-alist))
;; (autoload 'forth-block-mode "gforth2.el")
;; (setq auto-mode-alist (cons '("\\.fb\\'" . forth-block-mode)
;; 							auto-mode-alist))
;; (add-hook 'forth-mode-hook (function (lambda ()
;; 				       ;; customize variables here:
;; 				       (setq forth-indent-level 4)
;; 				       (setq forth-minor-indent-level 2)
;; 				       (setq forth-hilight-level 3)
;; 				       )))
;; (setq forth-help-load-path '("~/gf" "~/br" "~/usr/share/swiftforth"))
;; ; convert a C style #define to a constant
;; (setq forth-convert-c-define "^#define[	 ]\\(\\w+\\)[	 ]*\\([a-zA-Z0-9 +*\\(\\)]*\\)[	 ]*\\(.*\\)")

;; (defun forth-to-c-define ()
;;   (interactive)
;;   (query-replace-regexp forth-convert-c-define "\\3 constant \\1"))

(put 'upcase-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

;; (require 'xml-parse)
;; (setq exec-path nil)
(defun set-exec-path-from-shell-PATH ()
  "Set up Emacs' `exec-path' and PATH environment variable to match
that used by the user's shell.

This is particularly useful under Mac OS X and macOS, where GUI
apps are not started from a shell."
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string
			  "[ \t\n]*$" "" (shell-command-to-string
					  "$SHELL --login -c 'echo $PATH'"
						    ))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

;; (set-exec-path-from-shell-PATH)  

;; Syslog
;; (require 'syslog-mode)

;; Tail
;; (require 'tail)

;; MDFind
;; (require 'mdfind)

;; BASH
;; (autoload 'bashdb "bashdb" "BASH Debugger mode via GUD and bashdb" t)
;; (autoload 'mdb "mdb" "GNU make Debugger mode via GUD and mdb" t)

;; JavaScript
;; (add-to-list 'load-path "~/emacs/packages/js")
;; (autoload 'js-console "js-console" nil t)

;; Hide Lines
;; (require 'hide-lines)
;; (autoload 'hide-lines "hide-lines" "Hide lines based on a regexp" t)

;; (defun hide-lines-matching-selection ()
;;   (interactive)
;;   (let (
;; 	(selectedText (concat ".*" (buffer-substring (mark) (point)) ".*"))
;; 	)
;;     (hide-matching-lines selectedText)))

;; (global-set-key [C-c h] 'hide-lines)
;; (global-set-key [C-c u] 'show-all-invisible)
;; (global-set-key [C-c C-h] 'hide-lines-matching-selection)

;; Factor
;; (add-to-list 'load-path "~/emacs/fuel")
(setq factor-dir "~/factor/")
(setq fuel-dir (concat factor-dir  "misc/fuel"))
;; (setq fuel-dir (concat factor-dir  "~/emacs/fuel"))
(if (file-exists-p factor-dir)
    (progn
     (add-to-list 'load-path fuel-dir)
      (setq-default fuel-factor-root-dir factor-dir)
      (setq-default fuel-listener-factor-binary (concat factor-dir "factor"))
      (setq-default fuel-listener-factor-image (concat factor-dir "factor.image"))
      (setq factor-mode-use-fuel t)
      (require 'factor-mode)
      (require 'fuel-mode)

     ;; custom syntax
     (modify-syntax-entry ?$ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?@ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?? "w" factor-mode-syntax-table)
     (modify-syntax-entry ?_ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?: "w" factor-mode-syntax-table)
     (modify-syntax-entry ?< "w" factor-mode-syntax-table)
     (modify-syntax-entry ?> "w" factor-mode-syntax-table)
     (modify-syntax-entry ?. "w" factor-mode-syntax-table)
     (modify-syntax-entry ?, "w" factor-mode-syntax-table)
     (modify-syntax-entry ?& "w" factor-mode-syntax-table)
     (modify-syntax-entry ?| "w" factor-mode-syntax-table)
     (modify-syntax-entry ?% "w" factor-mode-syntax-table)
     (modify-syntax-entry ?= "w" factor-mode-syntax-table)
     (modify-syntax-entry ?/ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?+ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?* "w" factor-mode-syntax-table)
     (modify-syntax-entry ?- "w" factor-mode-syntax-table)
     (modify-syntax-entry ?\; "w" factor-mode-syntax-table)
     (modify-syntax-entry ?\' "w" factor-mode-syntax-table)
     (modify-syntax-entry ?^ "w" factor-mode-syntax-table)
     (modify-syntax-entry ?~ "w" factor-mode-syntax-table)

     ;; custom fuel keys
     (define-key factor-mode-map "\C-c\C-f" 'fuel-activate-factor)
     (define-key factor-mode-map [M-A-\' ] 'fuel-uncomment-fix)
     (define-key factor-mode-map "\C-c\C-e\C-h" 'fuel-add-help-word-template)
     (define-key fuel-mode-map (kbd "C-c p") 'fuel-eval-definition)
     (define-key fuel-mode-map (kbd "C-c <") 'fuel-show-callers)
     (define-key fuel-mode-map (kbd "C-c >") 'fuel-show-callees)
     (define-key fuel-mode-map (kbd "C-c r") 'fuel-refactor-inline-word)
     ;; (fset 'fuel-uncomment-fix
     ;;    [S-left S-left ?\A-\/ ?  down])
     (add-to-list 'interpreter-mode-alist '("factor" . factor-mode))

     (defun fuel-activate-factor ()
       (interactive)
       (mac-activate-process "Factor"))
     ))

(add-to-list 'load-path "~/.emacs.d/themes")
(require 'color-theme)
(require 'show-point-mode)
(show-point-mode t)

;; (require 'columnize)
;;    (autoload 'columnize-text "columnize"
;;              "Formats a list of items into columns (pillars)" t)
;;    (load "columnize" nil t)
;;
;; Optionally add a key mapping like this.
;; (global-set-key [?\C-x ?c] 'columnize-text)

;; (require 'align)

;; (require 'cisco-router-mode)

;; ;; WARNING!! don't execute anything below here if using plain old emacs
(if (string-equal "/usr/bin/emacs" (getenv "EMACS"))
    (top-level)) 

;; ;; (add-to-list 'load-path "~/emacs/packages/geben")
;; ;; (autoload 'geben "geben" "DBGp protocol frontend, a script debugger" t)

;; DNS
;; (setq auto-mode-alist (cons '("db\\..*" . dns-mode) auto-mode-alist))

;; (require 'multiple-cursors)
;; (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
;; (global-set-key (kbd "C->") 'mc/mark-next-like-this)
;; (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;; (global-set-key [C-c C-<] 'mc/mark-all-like-this)
;; (defun dos2unix (buffer)
;;   "Automate M-% C-q C-m RET C-q C-j RET"
;;   (interactive "*b")
;;   (save-excursion
;;     (goto-char (point-min))
;;     (while (search-forward (string ?\C-m) nil t)
;;       (replace-match (string ?\C-j) nil t))))

;; (setq tramp-default-method-alist
;; (quote
;;  ((nil "%" "smb")
;;   ("\\`\\(127\\.0\\.0\\.1\\|::1\\|localhost6?\\|pve\\)\\'" "\\`root\\'" "sudo")
;;   (nil "\\`\\(anonymous\\|ftp\\)\\'" "ftp")
;;   ("\\`ftp\\." nil "ftp"))))

 ;; ITERM2 MOUSE SUPPORT
    (unless window-system
      (require 'mouse)
      (xterm-mouse-mode t)
      (defun track-mouse (e))
      (setq mouse-sel-mode t)
    )


;; Desktop
;; (desktop-load-default)
;; (desktop-read)

;; ssh-config-mode
(autoload 'ssh-config-mode "ssh-config-mode" t)
(add-to-list 'auto-mode-alist '("/\\.ssh/config\\'"     . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/sshd?_config\\'"      . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/knownhosts\\'"       . ssh-known-hosts-mode))
(add-to-list 'auto-mode-alist '("/authorized_keys2?\\'" . ssh-authorized-keys-mode))
(add-hook 'ssh-config-mode-hook 'turn-on-font-lock)

;; make .source files use sh-mode
(add-to-list 'auto-mode-alist '("\\.source$" . shell-script-mode))

;; GraphViz
(require 'graphviz-dot-mode)

; don't make me type yes and no
(fset 'yes-or-no-p 'y-or-n-p)
;; (put 'narrow-to-region 'disabled nil)
(msb-mode t)
(load-theme 'tango-dark)

(frames-only-mode)
(other-frame-window-mode t)

(require 'visual-regexp-steroids)
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)

;; if you use multiple-cursors, this is for you:
;; (define-key global-map (kbd "C-c m") 'vr/mc-mark)

;; https://github.com/creichert/ido-vertical-mode.el
(ido-mode 1)
(setq ido-use-faces t)
(setq ido-grid-mode t)
(setq ido-vertical-show-count t)

(require 'ido-vertical-mode)

;; https://github.com/DarwinAwardWinner/mac-pseudo-daemon
(add-to-list 'load-path "~/emacs/contributed/mac-pseudo-daemon")
(require 'mac-pseudo-daemon)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(use-package company
    :defer 0.1
    :config
    (global-company-mode t)
    (setq-default
        company-idle-delay 0.05
        company-require-match nil
        company-minimum-prefix-length 0

        ;; get only preview
        company-frontends '(company-preview-frontend)
        ;; also get a drop down
        company-frontends '(company-pseudo-tooltip-frontend company-preview-frontend)
        ))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "TAB") #'company-complete-common-or-cycle)
  (define-key company-active-map (kbd "<tab>") #'company-complete-common-or-cycle)
  (define-key company-active-map (kbd "RET") #'company-complete-selection)
  (define-key company-active-map (kbd "<return>") #'company-complete-selection))

;(add-to-list 'load-path "~/emacs.d/codeium.el/codeium.el")
;(require 'codeium)
(use-package codeium
  ;; if you use straight
  :straight '(:type git :host github :repo "Exafunction/codeium.el")
  ;; otherwise, make sure that the codeium.el file is on load-path

  :init
  ;; use globally
  (add-to-list 'completion-at-point-functions #'codeium-completion-at-point)
  ;; or on a hook
  ;; (add-hook 'python-mode-hook
  ;;     (lambda ()
  ;;         (setq-local completion-at-point-functions '(codeium-completion-at-point))))
  
  ;; if you want multiple completion backends, use cape (https://github.com/minad/cape):
  ;; (add-hook 'python-mode-hook
  ;;     (lambda ()
  ;;         (setq-local completion-at-point-functions
  ;;             (list (cape-super-capf #'codeium-completion-at-point #'lsp-completion-at-point)))))
  ;; an async company-backend is coming soon!
  
  ;; codeium-completion-at-point is autoloaded, but you can
  ;; optionally set a timer, which might speed up things as the
  ;; codeium local language server takes ~0.2s to start up
  ;; (add-hook 'emacs-startup-hook
  ;;  (lambda () (run-with-timer 0.1 nil #'codeium-init)))
  
  ;; :defer t ;; lazy loading, if you want
  :config
  (setq use-dialog-box nil) ;; do not use popup boxes
  
  ;; if you don't want to use customize to save the api-key
  ;; (setq codeium/metadata/api_key "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
  
  ;; get codeium status in the modeline
  (setq codeium-mode-line-enable
        (lambda (api) (not (memq api '(CancelRequest Heartbeat AcceptCompletion)))))
  (add-to-list 'mode-line-format '(:eval (car-safe codeium-mode-line)) t)
  ;; alternatively for a more extensive mode-line
  ;; (add-to-list 'mode-line-format '(-50 "" codeium-mode-line) t)
  
  ;; use M-x codeium-diagnose to see apis/fields that would be sent to the local language server
  (setq codeium-api-enabled
        (lambda (api)
          (memq api '(GetCompletions Heartbeat CancelRequest GetAuthToken RegisterUser auth-redirect AcceptCompletion))))
  ;; you can also set a config for a single buffer like this:
  ;; (add-hook 'python-mode-hook
  ;;     (lambda ()
  ;;         (setq-local codeium/editor_options/tab_size 4)))
  
  ;; You can overwrite all the codeium configs!
  ;; for example, we recommend limiting the string sent to codeium for better performance
  (defun my-codeium/document/text ()
    (buffer-substring-no-properties (max (- (point) 3000) (point-min)) (min (+ (point) 1000) (point-max))))
  ;; if you change the text, you should also change the cursor_offset
  ;; warning: this is measured by UTF-8 encoded bytes
  (defun my-codeium/document/cursor_offset ()
    (codeium-utf8-byte-length
     (buffer-substring-no-properties (max (- (point) 3000) (point-min)) (point))))
  (setq codeium/document/text 'my-codeium/document/text)
  (setq codeium/document/cursor_offset 'my-codeium/document/cursor_offset))

;; Add copilot
;;add-to-list 'load-path "~/emacs/copilot.el")
;;(require 'copilot)                      
;;(with-eval-after-load 'copilot
;;  (define-key copilot-completion-map (kbd "A-<tab>") 'copilot-accept-completion)
;;  (define-key copilot-completion-map (kbd "C-<tab>") 'copilot-accept-completion-by-word)
;;  (define-key copilot-completion-map (kbd "A-M-<tab>") 'copilot-next-completion)
;;  (define-key copilot-completion-map (kbd "A-C-<tab>") 'copilot-previous-completion))
;;(add-hook 'prog-mode-hook 'copilot-mode)

;; Set the directory for backups
(setq backup-directory-alist `(("." . "~/.emacs.d/backups/")))
;; Set the directory for auto-saves
(setq auto-save-file-name-transforms `((".*" "~/.emacs.d/backups/" t)))


(add-hook 'after-init-hook 'global-company-mode)

(add-to-list 'load-path "~/.emacs.d/codeium.el")
(require 'codeium)

(defun open-in-xcode()
  (interactive)
  (shell-command (concat "open -a Xcode " (buffer-file-name))))
  
(global-set-key (kbd "M-X") 'open-in-xcode)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(setq magit-completing-read-function 'magit-ido-completing-read)

(setq server-socket-dir "~/.emacs.d/server")
(server-start)
;; ;; ;;; .emacs ends here

