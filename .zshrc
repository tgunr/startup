# This script defines some basic commands involved with shell nesting
# and the common help mechanism defined for use with related setup
# scripts.  Those scripts are used to further define the environment.
# There's ~/.userstartup which is used to establish some initial
# defaults, aliases, etc.  It's executed from here first prior to
# any scripts in the ~/.startup folder.  The scripts in the
# ~/.startup should be used to establish project specific
# conventions (see ~/.startup/.skeleton_statup_file for
# documentation - it's never executed, it's just a readme file
# illustrating what real .startup scripts should look like).

# I have also placed all the standard login files into .startup you
# should execute the following to establish links in your home directory.
# cd
# ./.startup/setlinks

#zmodload zsh/zprof
#zprof

export __startup_completed=0		# this is used to avoid repeating some inits

STARTDIR="$HOME/.startup"
ZDOTDIR="$STARTDIR/../zsh"
ZSH_DISABLE_COMPFIX=true

export DEBUG_ECHO=0
function echoIfDebug {
    if [[ $DEBUG_ECHO == 1 ]] ; then
	echo "$@"
    fi
}
echoIfDebug .zshrc $1 $2 $3  "\$PS1 = '$PS1'  \$-=$-  SHLVL = $SHLVL  SHELL = $SHELL"

export DISPLAY=localhost:0
#
## Initially $INITIAL_SHLVL is undefined.  We use it to capture what the initial
## shell level.  We need this to display our nesting info and to know if we
## are nesting shells.  If bash is not the default shell then intially SHLVL
## will be 2.  Otherwise it's 1.  This allows us to relativize our displays
## to the first bash invocation even if it's not the default shell.
#
if [ ! "$INITIAL_SHLVL" ]; then
    export INITIAL_SHLVL=$SHLVL
fi

#
## Report shell nesting level if we running a nested shell
#
if [ $SHLVL -gt  $INITIAL_SHLVL ]; then
   if [ "$SHLVL" != "$PREV_SHLVL" ]; then
   	echo "Pushing (level now $((SHLVL-INITIAL_SHLVL+1)))"
   fi
fi

if [ $INITIAL_SHLVL ]; then
    if [ $SHLVL -eq  $INITIAL_SHLVL ]; then
	    if [ -x /usr/local/bin/toilet ]; then
	        /usr/local/bin/toilet --metal -t $(hostname)
	    fi
	    if [[ $OSTYPE =~ "darwin" ]] ; then
	        ifconfig|grep "inet "|tail -n+2
	    else
	        if [ -f /usr/bin/screenfetch ]; then
	            /usr/bin/screenfetch -t
	        fi
	        ip address | grep "inet " | tail -n+2
	    fi
	    echo "DISPLAY: $DISPLAY"
    fi
fi

if [ -x /usr/bin/sw_vers ]; then
	sw_vers
else
	uname -srvm
fi

export HOMEBREW_GITHUB_API_TOKEN="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"
export HOMEBREW_TEMP=/usr/local/temp
export GWT_HOME=/Sources/gwt-eclipse-plugin
export GWT_VERSION=2.7.0
export GWT_ROOT=/usr/gwt/trunk
export GWT_TOOLS=/Sources/gwt-eclipse-plugin/tools/gwt/tools
export JDK_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home
export CLASSPATH=.:/usr/local/Cellar/antlr/4.7/antlr-4.7-complete.jar:$CLASSPATH
export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export FCEDIT=vi
export MORE=less
export LESS="-erX"
export PAGER=less
export PATH=~/usr/bin:/usr/local/bin:/Applications/Xcode/Contents/Developer/usr/bin:$PATH

if [[ $OSTYPE =~ "linux" ]] ; then
    export EDITOR=emacs
    #export EDITOR="emacsclient -s ~/.emacs.d/server/server"
    #export ALTERNATE_EDITOR="emacsclient -s $HOME/.emacs.d/server/server"
    export ALTERNATE_EDITOR=vi

elif [[ $OSTYPE =~ "darwin" ]] ; then
    export EDITOR=/Applications/Emacs.app/Contents/MacOS/Emacs
    export EDITOR="emacsclient -s ~/.emacs.d/server/server"
    export ALTERNATE_EDITOR=vi
else
    export EDITOR=vi
    export ALTERNATE_EDITOR=nano
fi

# export LDFLAGS="-L/usr/local/opt/icu4c/lib"
# export CPPFLAGS="-I/usr/local/opt/icu4c/include"
# export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig"
# export GAE_HOME=<path to GAE SDK> i.e. /opt/appengine-java-sdk-1.9.6
# PERL5LIB="/Users/davec/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
# PERL_LOCAL_LIB_ROOT="/Users/davec/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
# PERL_MB_OPT="--install_base \"/Users/davec/perl5\""; export PERL_MB_OPT;
# PERL_MM_OPT="INSTALL_BASE=/Users/davec/perl5"; export PERL_MM_OPT;
# [ $SHLVL -eq 1 ] && eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"
# source ~/perl5/perlbrew/etc/bashrc
export ZSH="$HOME/.oh-my-zsh"
source "$HOME/.ohmyzshrc"

#ZSH_COLORIZE_STYLE="colorful"
ZAQ_PREFIXES=('git commit -m' 'ssh *')
ZAQ_PREFIXES+=('gits ci -m' 'ssh *')
ZAQ_PREFIXES+=('git commit( [^ ]##)# -[^ -]#m')
ZAQ_PREFIXES+=('gits ci( [^ ]##)# -[^ -]#m')
ZAQ_PREFIXES+=('ssh( [^ ]##)# [^ -][^ ]#')

DIRSTACKSIZE=10
setopt autopushd
setopt pushdminus
setopt pushdsilent
setopt pushdtohome
setopt caseglob
setopt globcomplete
setopt extended_history
setopt inc_append_history
setopt share_history
setopt append_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_find_no_dups
setopt hist_reduce_blanks
setopt hist_verify
setopt correct

alias cd\?='dirs -v'

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#

# Extras
autoload -Uz compinit
compinit -C

export ITERM2_SUPPORT="t"               # enable iterm2 support for cli integration
export ITERM_ENABLE_SHELL_INTEGRATION_WITH_TMUX=YES
test -e ${HOME}/.iterm2_shell_integration.zsh && source ${HOME}/.iterm2_shell_integration.zsh

### Key Bindings
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward


# Set fzf installation directory path
# export FZF_BASE=/usr/local/Cellar/fzf/0.21.0-1

autoload -Uz compinit && compinit

# Uncomment the following line to disable fuzzy completion
# export DISABLE_FZF_AUTO_COMPLETION="true"

# Uncomment the following line to disable key bindings (CTRL-T, CTRL-R, ALT-C)
# export DISABLE_FZF_KEY_BINDINGS="true"

# User configuration
# if [ -e /usr/local/opt/fzf/shell/key-bindings.zsh ] ; then
#     [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# fi

if [[ "$TERM" == "dumb" ]]; then
	PROMPT='# '
	unsetopt zle
	unsetopt prompt_cr
	unsetopt prompt_subst
	# unfunction precmd # these two are not
	# unfunction preexec # working for me
	PS1='$ '
else
    autoload -Uz promptinit
    promptinit
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        if [[ "$USER" == root ]]; then 
            prompt adam1 orange
        else
	    prompt adam1 yellow
        fi
    else
        if [[ "$USER" == root ]]; then 
            prompt adam1 red
        else
            prompt adam1 blue
        fi
    fi    
    # PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{blue}%3~%f%b %# '
    # PROMPT='%B%F{blue}%3~%f%b %# '
    autoload -Uz vcs_info
    precmd_vcs_info() { vcs_info }
    precmd_functions+=( precmd_vcs_info )
    setopt prompt_subst
    RPROMPT=\$vcs_info_msg_0_
    zstyle ':vcs_info:git:*' formats '%F{240}(%b)%r%f'
    zstyle ':vcs_info:*' enable git
fi

#------------------------------------------------------------------------------------------#
# We always try to execute ~/.userstartup first to defined global user settings.
# What's the point of doing all this if we're not interactive?
if [ "$TERM" != "dumb" ] ; then
    if [ ! "`echo $- | grep i`" = "" ]; then
	    if [ -f ~/.userstartup ]; then			# execute ~/.userstartup first
	        echoIfDebug "source .userstartup"
	        source ~/.userstartup
	    fi
    fi
fi

unset f
#------------------------------------------------------------------------------------------#
#
# Wait until all .startup scripts are run to do the cd to set the 1st prompt.
# We need to wait since startups could define ~userid prefixes for pathnames
# with define_known_directory calls and we might be "in" one of those directories
# when .bashrc is executed.

cd "$PWD"

export PREV_SHLVL=$SHLVL
export __startup_completed=1

export NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

[ -f /opt/homebrew/etc/profile.d/autojump.sh ] && . /opt/homebrew/etc/profile.d/autojump.sh

if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

export PATH=/opt/homebrew/opt/python@3.10/libexec/bin:$PATH

# # begin forge completion
# . <(forge --completion)
# # end forge completion

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/davec/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/davec/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/davec/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/davec/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

export OPENAI_API_KEY="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"
export OPENAI_API_ORG="org-J0C3Km7cUgI3UgXuNvnmrUFr"

alias reez='source ~/.zshrc'

[[ "$TERM_PROGRAM" == "CodeEditApp_Terminal" ]] && . "/Applications/CodeEdit.app/Contents/Resources/codeedit_shell_integration.zsh"
export PYTHONPATH=/opt/homebrew/Cellar/scons/4.7.0/libexec/lib/python3.12/site-packages
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export OLLAMA_HOST=0.0.0.0
export PATH="$PATH:/Applications/010 Editor.app/Contents/CmdLine" #ADDED BY 010 EDITOR
export PATH="/opt/homebrew/opt/util-linux/bin:$PATH"
export PATH="/opt/homebrew/opt/util-linux/sbin:$PATH"
export PATH="/opt/homebrew/opt/make/libexec/gnubin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export COLUMNS="120"
export PATH="$PATH:$HOME/Applications/ACLI"

# Remove the interactive rm alias
unalias rm 2>/dev/null || true
# The following lines have been added by Docker Desktop to enable Docker CLI completions.
fpath=(/Users/davec/.docker/completions $fpath)
autoload -Uz compinit
compinit
# End of Docker CLI completions

# Added by Windsurf
export PATH="/Users/davec/.codeium/windsurf/bin:$PATH"
