#!/bin/bash
#echo .bashrc $@
# logger "bash: .bashrc..."
# 	File:		$Id: .bashrc,v 1.7 2004/09/14 19:23:08 davec Exp $
#
# 	Contains:	BASH initialization code
#
# 	Copyright:	© 2003 by Apple Computer, Inc., all rights reserved.
#
# 		$Log: .bashrc,v $
# 		Revision 1.7  2004/09/14 19:23:08  davec
# 		Updated from local.
#
# 		Revision 1.6  2003/04/25 06:41:36  davec
# 		Changed .startup_items to .startup
#
# 		Revision 1.5  2003/04/17 07:36:42  davecl
# 		Copied oftest repository to my /CVS and merged in local changes.
#
# 		Revision 1.4  2003/03/13 17:30:46  davec
# 		Added header.
#
#
# Invoked as an interactive login shell, or with `--login'
# ........................................................
# When Bash is invoked as an interactive login shell, or as a
# non-interactive shell with the `--login' option, it first reads and
# executes commands from the file `/etc/profile', if that file exists.
# After reading that file, it looks for `~/.bash_profile',
# `~/.bash_login', and `~/.profile', in that order, and reads and
# executes commands from the first one that exists and is readable.  The
# `--noprofile' option may be used when the shell is started to inhibit
# this behavior.
#    When a login shell exits, Bash reads and executes commands from the
# file `~/.bash_logout', if it exists.
#
# Invoked as an interactive non-login shell
# .........................................
#
# When an interactive shell that is not a login shell is started, Bash
# reads and executes commands from `~/.bashrc', if that file exists.
# This may be inhibited by using the `--norc' option.  The `--rcfile
# FILE' option will force Bash to read and execute commands from FILE
# instead of `~/.bashrc'.
#
#    So, typically, your `~/.bash_profile' contains the line
#      `if [ -f ~/.bashrc ]; then . ~/.bashrc; fi'
#    after (or before) any login-specific initializations.
#
# Invoked non-interactively
# .........................
# When Bash is started non-interactively, to run a shell script, for
# example, it looks for the variable `BASH_ENV' in the environment,
# expands its value if it appears there, and uses the expanded value as
# the name of a file to read and execute.  Bash behaves as if the
# following command were executed:
#      `if [ -n "$BASH_ENV" ]; then . "$BASH_ENV"; fi'
#    but the value of the `PATH' variable is not used to search for the
# file name.
#    As noted above, if a non-interactive shell is invoked with the
# `--login' option, Bash attempts to read and execute commands from the
# login shell startup files.


# If bash is the initial login shell then we have ~/.bash_login source
# in ~/.bashrc.  When bash is not a login shell the ~/.bashrc is always
# executed.

# If [t]csh is the initial shell then either ~/.cshrc or ~/.login will
# invoke bash replacing [t]csh as the current shell and thus still
# causing ~/.bashrc to be executed.

# This script defines some basic commands involved with shell nesting
# and the common help mechanism defined for use with related setup
# scripts.  Those scripts are used to further define the environment.
# There's ~/.userstartup which is used to establish some initial
# defaults, aliases, etc.  It's executed from here first prior to
# any scripts in the ~/.startup folder.  The scripts in the
# ~/.startup should be used to establish project specific
# conventions (see ~/.startup/.skeleton_statup_file for
# documentation - it's never executed, it's just a readme file
# illustrating what real .startup scripts should look like).

# I have also placed all the standard login files into .startup you
# should execute the following to establish links in your home directory.
# cd
# ln -s .startup/.login
# ln -s .startup/.profile
# ln -s .startup/.bashrc
# ln -s .startup/.userstartup
# ln -s .startup/.emacs

#------------------------------------------------------------------------------------------#
#
## These folllowing two aliases are defined first to control set -x tracing.
## In certain places, like here, we always want to disable tracing.  Use
## __push_trace to save trace state and turn it off and __pop_trace to restore
## tracing if it was on at the time of the __push_trace.  As just mentioned,
## these are only used in selected places and then where we know we are doing a
## top-level command since this push is not done to a stack.
#

# don't wait to print exit status of background jobs, don't exit on Ctrl-D
#set -b -o ignoreeof

STARTDIR="$HOME/ownCloud/Shell/startup"

__push_trace () {
    __restore_set=$-
    builtin set +x
    __restore_set="`echo $__restore_set | fgrep x`"
}

__push_trace				# preserve set flags

__pop_trace () {
    if [ "$__restore_set" ]; then
	builtin set -x
    fi
}

export DEBUG_ECHO=0
function echoIfDebug {
    if [[ $DEBUG_ECHO == 1 ]] ; then
	echo "$@"
    fi
}

echoIfDebug .bashrc $1 $2 $3  "\$PS1 = '$PS1'  \$-=$-  SHLVL = $SHLVL  SHELL = $SHELL"

if [[ $OSTYPE =~ "darwin" ]] ; then
    # First setup PATH using system tool
    # echo "PATH: $PATH"

export PATH=\
~/bin:\
~/perl5/bin:\
~/.pyenv/bin:\
/opt/usr/bin:\
/usr/local/opt/icu4c/bin:\
/usr/local/opt/python/libexec/bin:\
/usr/local/opt/coreutils/libexec/gnubin:\
/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home/bin:\
/Library/Developer/Toolchains/swift-latest.xctoolchain/usr/bin:\
~/src/pyenv/libexec:\
~/bin/git-scripts:\
$PATH

export MANPATH=\
/usr/local/opt/coreutils/libexec/gnuman:\
$MANPATH

export INFOPATH=\
$INFOPATH

    # echo "PATH:$PATH"|tr ":" "\n"

    export HOMEBREW_GITHUB_API_TOKEN="882097152ea2016a6e7364b35748bdf32a527f01"
    export HOMEBREW_TEMP=/usr/local/temp
    export GWT_HOME=/Sources/gwt-eclipse-plugin
    export GWT_VERSION=2.7.0
    export GWT_ROOT=/usr/gwt/trunk
    export GWT_TOOLS=/Sources/gwt-eclipse-plugin/tools/gwt/tools
    # export GAE_HOME=<path to GAE SDK> i.e. /opt/appengine-java-sdk-1.9.6
    export JDK_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home
    export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home
    export CLASSPATH=.:/usr/local/Cellar/antlr/4.7/antlr-4.7-complete.jar:$CLASSPATH

    # PERL5LIB="/Users/davec/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
    # PERL_LOCAL_LIB_ROOT="/Users/davec/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
    # PERL_MB_OPT="--install_base \"/Users/davec/perl5\""; export PERL_MB_OPT;
    # PERL_MM_OPT="INSTALL_BASE=/Users/davec/perl5"; export PERL_MM_OPT;
    # [ $SHLVL -eq 1 ] && eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"
    # source ~/perl5/perlbrew/etc/bashrc

    # dispdir=`dirname $DISPLAY`
    # dispfile=`basename $DISPLAY`
    # dispnew="$dispdir/:0"
    # if [ -e $DISPLAY -a "$dispfile" = "org.x:0" ]; then
    # 	mv $DISPLAY $dispnew
    # fi
    export DISPLAY=:0
    export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
    export CC=/usr/bin/gcc
    export CXX=/usr/bin/g++
#     eval "$(pyenv init -)"
#     eval "$(pyenv virtualenv-init -)"
#     eval "$(pyenv init -)"
else
# All OS get these
export PATH=\
~/bin:\
/snap/bin:\
$PATH

export MANPATH=\
~/usr/man:\
/usr/share/man:\
/usr/local/share/man:\
/opt/X11/share/man

export INFOPATH=\
~/usr/info:\
$INFOPATH

fi

export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
# export LDFLAGS="-L/usr/local/opt/icu4c/lib"
# export CPPFLAGS="-I/usr/local/opt/icu4c/include"
# export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig"

# Editor
if [[ $OSTYPE =~ "linux" ]] ; then
    export EDITOR=emacs
    #export EDITOR="emacsclient -s ~/.emacs.d/server/server"
    #export ALTERNATE_EDITOR="emacsclient -s $HOME/.emacs.d/server/server"
    export ALTERNATE_EDITOR=vi

elif [[ $OSTYPE =~ "darwin" ]] ; then
    export EDITOR=/Applications/Aquamacs.app/Contents/MacOS/Aquamacs
    export ALTERNATE_EDITOR=vi
else
    export EDITOR=vi
    export ALTERNATE_EDITOR=nano
fi
export FCEDIT=vi

export HISTCONTROL=ignoreboth		# dups and lines beginning with space
export HISTFILESIZE=500
export HISTSIZE=500
export HISTIGNORE="&:ls:l:la:ltr:h:[bf]g:exit"

export MINICOM='-m -c on'
export PDSH_RCMD_TYPE=ssh

#export TERM=Apple_Terminal
#export TERM=xterm-color
#export TERM=vt100
#export TERM=nsterm

# For ls and others, define we want COLOR!
export CLICOLOR=1

# The TERM variable can be set in the Terminal Preferences also, but it won't turn on the colorlabeling until you add at least the CLIcolor variable to your .tcshrc file.

# Read the rest of the hint for more on color definitions.

# The colors can be set with the LScolorS variable. The color designators are as follows:
# a     black
# b     red
# c     green
# d     brown
# e     blue
# f     magenta
# g     cyan
# h     light grey
# A     bold black, usually shows up as dark grey
# B     bold red
# C     bold green
# D     bold brown, usually shows up as yellow
# E     bold blue
# F     bold magenta
# G     bold cyan
# H     bold light grey; looks like bright white
# x     default foreground or background
# The color designators can also be as follows:

# Note that the above are standard ANSI colors. The actual display may differ depending on the color capabilities of the terminal in use.

# The order of the attributes are as follows:
# 1. directory
# 2. symbolic link
# 3. socket
# 4. pipe
# 5. executable
# 6. block special
# 7. character special
# 8. executable with setuid bit set
# 9. executable with setgid bit set
# 10. directory writable to others, with sticky bit
# 11. directory writable to others, without sticky bit

# The default is "exfxcxdxbxegedabagacad",
# i.e. blue foreground and default background for regular directories,
# black foreground and red background for setuid executables, etc.

export LS_COLORS="di=$fgCyan:ln=$fgRed:so=$fgCyan:bd=$fgGreen:cd=$fgYellow:ex=$fgMagenta"
# export LS_COLORS="no=00;$fgGreen:fi=00:di=00;$fgCyan:ln=01;$fgCyan:pi=04;$fgYellow:so=01;$fgMagenta:bd=33;04:cd=33;04: or=31;01:ex=00;32:*.rtf=00;33:*.txt=00;33:*.html=00;33:*.doc=00;33:*.pdf=00;33: *.ps=00;33:*.sit=00;31:*.hqx=00;31:*.bin=00;31:*.tar=00;31:*.tgz=00;31:*.arj=00;31: *.taz=00;31:*.lzh=00;31:*.zip=00;31:*.z=00;31:*.Z=00;31:*.gz=00;31:*.deb=00;31: *.dmg=00;36:*.jpg=00;35:*.gif=00;35:*.bmp=00;35:*.ppm=00;35:*.tga=00;35: *.xbm=00;35:*.xpm=00;35:*.tif=00;35:*.mpg=00;37:*.avi=00;37:*.gl=00;37:*.dl=00;37: *.mov=00;37:*.mp3=00;35:"
# export LS_COLORS="Hefxcxdxbxegedabagacad"
#----------------------------------------------------------------------------------------#

#
## Some configurable settings to change the prompting behavior slightly
#
if [ "$HOSTNAME" = "localhost" ]; then
    export DEFAULT_HOST='localhost'	# don't show host if we're local
else
    export DEFAULT_HOST='$USER'		# don't show host if we're on this host
fi

export TWO_LINE_LONG_PROMPT=0		# prompts > MAX_PROMPT_WIDTH show as two lines
export ALWAYS_TWO_LINE_PROMPT=0		# always use a two-line prompt no matter the length
export MAX_PROMPT_WIDTH=34 # 44		# max prompt width before using two lines
export SUPPORT_COLOR=1			# add color or shading where appropriate
export MAX_CD_STACK_DEPTH=35		# max nbr of remembered cd directories
export ITERM2_SUPPORT="t"               # enable iterm2 support for cli integration

# Note SUPPORT_COLOR is also an environment variable for cp/rm/mv if the script
# "{cp,mv,rm}" is in the .startup to use extended versions of those commands.
# See script for details.
alias RedTerm='aterm -tint red -tr -trsb -sh 70 -cr green +sb -sl 1000 -title Nosf3ratu\ Term -fn 8x13 -fg White'
alias BlueTerm='aterm -tint blue -tr -trsb -sh 70 -cr green +sb -sl 1000 -title Nosf3ratu\ Term -fn 8x13 -fg White'
alias GreenTerm='aterm -tint green -tr -trsb -sh 40 -cr green +sb -sl 1000 -title Nosf3ratu\ Term -fn 8x13 -fg White'
alias BlackTerm='aterm -tint black -tr -trsb -sh 90 -cr green +sb -sl 1000 -title Nosf3ratu\ Term -fn 8x13 -fg White'

# MY_MACHINE is 1 if this machine is considered "mine", i.e., HOSTNAME is a "localhost"
# or the DEFAULT_HOST (the one you don't want to show in command line prompts).  MY_MACHINE
# can be used in (install and/or user startup) scripts to decide whether to modify or add
# stuff to places outside the home directory tree.  For example, you might not want to
# install stuff in /usr/local/bin when running on some one else's machine but you would
# on your own machine.

if [ "$HOSTNAME" = "$DEFAULT_HOST" ]; then
    export MY_MACHINE=1
else
    export MY_MACHINE=0
fi

# Subversion needs this
#export LC_CTYPE=en_US.UTF-8
export SVN_SSH="ssh -i $HOME/.ssh/svn_dsa"

export MORE=less
export LESS="-erX"
export PAGER=less

complete -c openman

#
## The bash $HOME and what /bin/pwd produce are not always exactly the same.
## I don't know whay this is so (but I think it's because there could be symlinks
## or mount points within the pathname).  So in order to be able to reliably test
## for the home directlry we check both $HOME and the variable set below which
## is what /bin/pwd produces.
#

pwd="$PWD"
builtin cd ~
export TRUE_HOME="`/bin/pwd`"
builtin cd "$pwd"
unset pwd

# The following is stuff I ripped off from other startups...
umask 022

shopt -s cdspell			# hey, why not? :-)
shopt -s histverify
shopt -s histappend; PROMPT_COMMAND='history -a'

#
## Initially $INITIAL_SHLVL is undefined.  We use it to capture what the initial
## shell level.  We need this to display our nesting info and to know if we
## are nesting shells.  If bash is not the default shell then intially SHLVL
## will be 2.  Otherwise it's 1.  This allows us to relativize our displays
## to the first bash invocation even if it's not the default shell.
#
if [ ! "$INITIAL_SHLVL" ]; then
    export INITIAL_SHLVL=$SHLVL
fi

#
## Report shell nesting level if we running a nested shell
#
#if [ $SHLVL -gt  $INITIAL_SHLVL ]; then
#    if [ "$SHLVL" != "$PREV_SHLVL" ]; then
#    	echo "Pushing (level now $((SHLVL-INITIAL_SHLVL+1)))"
#    fi
#fi

# Find out where bash lives...
#
# Enough of this crap!  The 'which' script is a [t]csh script which causes a
# non-interactive (I think) [t]csh startup sequence.  That will mean it may
# go through our [t]csh startup scripts which may have called the bash startup
# scripts in the first place.  It all gets very confusing both to me and
# apparently to the damn shell's under certain conditions.  So to avoid all
# this we hunt down bash ourselves using a set of default search locations.

#export SHELL=`which bash`
found=
for SHELL in /bin /usr/local/bin /usr/bin /sbin /usr/sbin ~/bin; do
    if [ -x "$SHELL/bash" -a ! -d "$SHELL/bash" ]; then
    	export SHELL="$SHELL/bash"
    	found=1
    	break
    fi
done
if [ ! "$found" ]; then
    echo "Cannot determine where bash is (???)"
    export SHELL="<how did we get here>"
fi
unset found

export __startup_completed=0		# this is used to avoid repeating some inits

#------------------------------------------------------------------------------------------#

#
## exit - intercept exit so we can give feedback on the shell nesting depth
#
exit()					# just to trace nested shell exits
{
    if false; then			# don't do when using "keychain"
    if [ "$SSH_AGENT_PID" != "" ]; then
    	kill -9 $SSH_AGENT_PID 2> /dev/null
	command rm -rf "`echo $SSH_AUTH_SOCK | sed -e 's/^\(.*\)'"${SSH_AUTH_SOCK##/*/}"'/\1/'`" 2> /dev/null
	export -n SSH_AUTH_SOCK SSH_AGENT_PID
	unset SSH_AUTH_SOCK SSH_AGENT_PID
    fi
    fi

    if [ $SHLVL -gt $INITIAL_SHLVL ]; then
    	if [ $((SHLVL-1)) -gt $INITIAL_SHLVL ]; then
	    echo "Popping nested shell level $((SHLVL-INITIAL_SHLVL+1))->$((SHLVL-INITIAL_SHLVL)) (still nested)"
	else
	    echo "Popping nested shell level $((SHLVL-INITIAL_SHLVL+1))-->$((SHLVL-INITIAL_SHLVL)) (no longer nested)"
	fi
    fi

    builtin exit
}

#------------------------------------------------------------------------------------------#

#
## nested - command to display the current shell nesting level
#
nested()				# for checking if we're in a nested shell
{
    if [ $SHLVL -gt  $INITIAL_SHLVL ]; then
	echo "yes ($((SHLVL-INITIAL_SHLVL)) down, i.e., level $((SHLVL-INITIAL_SHLVL+1)))"
    else
    	echo "no"
    fi
}


#------------------------------------------------------------------------------------------#
#
## The file ~/.userstartup should contain common definitions used for all projects.
## Note, there is one special .startup script called "(directory-commands)" which
## is generally required.
#
define_known_directory()			# in case (directory-commands) isn't defined
{
    echo "\"(directory-commands)\" is not installed in ~/.startup-items"
}

#------------------------------------------------------------------------------------------#
# We always try to execute ~/.userstartup first to defined global user settings.
# What's the point of doing all this if we're not interactive?
if [ "$TERM" != "dumb" ] ; then
    if [ ! "`echo $- | grep i`" = "" ]; then

	    if [ -f ~/.userstartup ]; then			# execute ~/.userstartup first
	        echoIfDebug "source .userstartup"
	        source ~/.userstartup
	    fi
    fi
fi
	# Any (project specific?) startup files may be placed in the ~/.startup folder.
	# All files in this directory that are not .-files or directories are executed.
	# Links are allowed.  Blanks are not allowed in the filenames.
            
unset f
export __startup_completed=1

#------------------------------------------------------------------------------------------#
#
# Wait until all .startup scripts are run to do the cd to set the 1st prompt.
# We need to wait since startups could define ~userid prefixes for pathnames
# with define_known_directory calls and we might be "in" one of those directories
# when .bashrc is executed.

cd "$PWD"

export PREV_SHLVL=$SHLVL

__pop_trace

# Change the color palette

# ^[]Pnrrggbb^[\

# Replace "n" with:

# 0-f (hex) = ansi color
# g = foreground
# h = background
# i = bold color
# j = selection color
# k = selected text color
# l = cursor
# m = cursor text
# rr, gg, bb are 2-digit hex value (for example, "ff"). Example in bash that changes the foreground color blue:

# echo -e "\033]Pg4040ff\033\\"

function ct () {
if [[ $# == 3 ]] ; then
    C1=$(printf '%02x' $1 )
    C2=$(printf '%02x' $2 )
    C3=$(printf '%02x' $3 )
    echo -e "\033]Ph$C1$C2$C3\033\\"
else
    echo "ct R G B"
fi
}

function ctab () {
if [[ $# == 3 ]] ; then
    C1=$(printf '%d' $1 )
    C2=$(printf '%d' $2 )
    C3=$(printf '%d' $3 )
    echo -e "\033]6;1;bg;red;brightness;$C1\a"
    echo -e "\033]6;1;bg;green;brightness;$C2\a"
    echo -e "\033]6;1;bg;blue;brightness;$C3\a"
else
    echo "ctab R G B"
fi
}

white='255 255 255'
canatloupe='255 204 102'
honeydew='204 255 102'
spindrift='102 255 204'
sky='102 204 255'
lavender='204 102 255'
carnation='255 111 207'
salmon='255 102 102'
banana='255 255 102'
flora='102 255 102'
ice='102 255 255'
orchid='128 255 0'
bubblegum='0 255 128'
tangerine='255 128 0'
lime='128 255 0'
seafoam='0 255 128'
aqua='0 128 255'
grape='128 0 255'
strawberry='255 0 128'
cherry='255 0 0'
lemon='255 255 0'
spring='0 255 0'
turquoise='0 255 255'
blueberry='92 92 255'
magenta='255 0 255'
mercury='230 230 230'
silver='204 204 204'
magnesium='179 179 179'
aluminum='153 153 153'
nickle='128 128 128'

alias term0="ct $white"
alias term1="ct $canatloupe"
alias term2="ct $honeydew"
alias term3="ct $spindrift"
alias term4="ct $sky"
alias term5="ct $lavender"
alias term6="ct $carnation"
alias term7="ct $salmon"
alias term8="ct $banana"
alias term9="ct $flora"
alias term10="ct $ice"
alias term11="ct $orchid"
alias term12="ct $bubblegum"
alias term13="ct $tangerine"
alias term14="ct $lime"
alias term15="ct $seafoam"
alias term16="ct $aqua"
alias term17="ct $grape"
alias term18="ct $strawberry"
alias term19="ct $cherry"
alias term20="ct $lemon"
alias term21="ct $spring"
alias term22="ct $turquoise"
alias term23="ct $blueberry"
alias term24="ct $magenta"
alias term25="ct $mercury"
alias term26="ct $silver"
alias term27="ct $magnesium"
alias term28="ct $aluminum"
alias term29="ct $nickle"

alias twhite=term0
alias tcanatloupe=term1
alias thoneydew=term2
alias tspindrift=term3
alias tsky=term4
alias tlavender=term5
alias tcarnation=term6
alias tsalmon=term7
alias tbanana=term8
alias tflora=term9
alias tice=term10
alias torchid=term11
alias tbubblegum=term12
alias ttangerine=term13
alias tlime=term14
alias tseafoam=term15
alias taqua=term16
alias tgrape=term17
alias tstrawberry=term18
alias tcherry=term19
alias tlemon=term20
alias tspring=term21
alias tturquoise=term22
alias tblueberry=term23
alias tmagenta=term24
alias tmercury=term25
alias tsilver=term26
alias tmagnesium=term27
alias taluminum=term28
alias tnickle=term29

function rct {
    R_COLOR=$(( $RANDOM % 30 ))
    RTERM="term$R_COLOR"
    eval $RTERM
}

export ITERM_ENABLE_SHELL_INTEGRATION_WITH_TMUX=YES
alias osx-setup='/root/OSX-PROXMOX/setup'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
