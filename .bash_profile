#!/bin/bash
# echo .bash_profile $1 $2 $3
#logger "bash: .bash_profile..."
# 	File:		$Id: .bashrc,v 1.6 2003/04/25 06:41:36 davec Exp $
#
# 	Contains:	BASH initialization code
#
# This file gets executed only when bash is a login shell.

# If bash is the initial login shell then we have ~/.bash_login source
# in ~/.bashrc.  When bash is not a login shell the ~/.bashrc is always
# executed.

# If [t]csh is the initial shell then either ~/.cshrc or ~/.login will
# invoke bash replacing [t]csh as the current shell and thus still
# causing ~/.bashrc to be executed.

# This script defines some basic commands involved with shell nesting
# and the common help mechanism defined for use with related setup
# scripts.  Those scripts are used to further define the environment.
# There's ~/.userstartup which is used to establish some initial
# defaults, aliases, etc.  It's executed from here first prior to
# any scripts in the ~/.startup folder.  The scripts in the
# ~/.startup should be used to establish project specific
# conventions (see ~/.startup/.skeleton_statup_file for
# documentation - it's never executed, it's just a readme file
# illustrating what real .startup scripts should look like).

#------------------------------------------------------------------------------------------#
#
## These folllowing two aliases are defined first to control set -x tracing.
## In certain places, like here, we always want to disable tracing.  Use
## __push_trace to save trace state and turn it off and __pop_trace to restore
## tracing if it was on at the time of the __push_trace.  As just mentioned,
## these are only used in selected places and then where we know we are doing a
## top-level command since this push is not done to a stack.

### NOTICE!!!
# Do not place anything in .bashrc which will echo back to terminal or many non-interactive
# things will not work properly. Things like `ssh me@somehost.com ls some/path' would return
# extra text which is probably not what you desire. Any thing you want echoed back should be
# placed here in .bash_profile

export __INTERACTIVE_SHELL__=1

# # What's the point of doing all this if we're not interactive?
# if [ "$TERM" != "dumb" ] ; then
# 	if [ "`echo $- | grep i`" = "" ]; then
# 		return
# 	fi
# fi

# stty erase "^?" intr "^C" kill "^U" -decctlq
# TERM=`tset -s`
# export TERM

source $HOME/.bashrc

# Display some system info if this is the initial bash login...

if [ $INITIAL_SHLVL ]; then
    if [ $SHLVL -eq  $INITIAL_SHLVL ]; then
	if [ -x /usr/local/bin/toilet ]; then
	    /usr/local/bin/toilet --metal -t $(hostname)
	fi
	if [ -f /usr/bin/screenfetch ]; then
	    /usr/bin/screenfetch -t
	fi
	if [[ $OSTYPE =~ "darwin" ]] ; then
	    ifconfig|grep inet|head -n 1
	else
	    ip address | grep inet
	fi
	echo "DISPLAY: $DISPLAY"
    fi
fi

unset f
export __startup_completed=1
echo "User startup complete"

#------------------------------------------------------------------------------------------#

# Wait until all .startup scripts are run to do the cd to set the 1st prompt.
# We need to wait since startups could define ~userid prefixes for pathnames
# with define_known_directory calls and we might be "in" one of those directories
# when .bashrc is executed.

cd "$PWD"

export PREV_SHLVL=$SHLVL

#__pop_trace

#echo "Skipping .rvm"
# [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOM=E/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
test -e ${HOME}/.iterm2_shell_integration.bash && source ${HOME}/.iterm2_shell_integration.bash

if [ -e /usr/local/bin/brew ]; then
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
	. $(brew --prefix)/etc/bash_completion
    fi
fi
export PATH="$HOME/bin:/usr/local/opt/gettext/bin:$HOME/.cargo/bin:$PATH"
TZ='America/Chicago'; export TZ

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

