#
## nfsclean [dir ...] - remove the "nfs turds" from directories
##
## Moves any nfs "turds" (.nfsXXXX files) to ~/.nfs-turds.  Given a (list of)
## directories, it will clean the entire hierarchy rooted in those directories.
## With no options, it cleans ONLY the current directory.  To recurse from the
## current directory specify '.' as an explicit starting directory.
#
nfsclean()
{
    local f
    
    if [ ! -d ~/.nfs-turds ]; then
	mkdir -p ~/.nfs-turds
    fi
    
    if [ $# = 0 ]; then
    	for f in `command ls .nfs* 2> /dev/null`; do # this does whan find does but not recurse
    	    if [ "$f" != ".nfs-turds" ]; then
    	    	echo "$f"
    	    	command mv "$f" ~/.nfs-turds
    	    fi
    	done
    else
	find "$@" \( -name .nfs-turds -prune \) -or \
        	  \( -type f -name ".nfs*" -print -exec mv {} ~/.nfs-turds \; \)
    fi
    
    return 0				# just in case
}

command rm -rf ~/.nfs-turds		# always start with no turds if possible

