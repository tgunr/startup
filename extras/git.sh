### GIT
alias gitdiff='git diffgit'
alias gitnew='git init && git add . && git commit -a -m "First Edition"'
alias gitst='git status'
alias bfg='java -jar $HOME/bin/bfg.jar'

function gitci {
    args=
    while [[ "${1:0:1}" == "-" ]] ; do
	    args="$args $1"
	    shift
    done
    if [[ -e "$1" ]] ; then
	    file="$1"
	    shift
	    git commit $args -m "$@" "$file"
    else
	    echo "$1 not found"
    fi
}

function gitignorecallback {
	grep -qs $1 .gitignore
	if [[ $? != 0 ]] ; then
		echo "Adding $1 to ignore list"
		echo $1 >> .gitignore
	fi
}

function gitignore {
	mapargs gitignorecallback "$@"
}

function gitstop {
	argoptions=""
	if [[ "$1" == -* ]] ; then
		argoptions=$1
		shift
	fi
	(gitignore $@)
	(git rm --cached $argoptions $@)
}

# Use Git’s colored diff when available
hash git &>/dev/null;
if [ $? -eq 0 ]; then
	function diff() {
		git diff --no-index --color-words "$@";
	}
fi;

function git-add-as-submodule {
    local new="$1"
    pushd "$new"
    local url=$(git remote get-url $(git remote))
    popd
    echo "Adding $new as submodule"
    git submodule add $url "$new"
}

function gitexport {
    local new="$1"
    if [ -d "$new" ] ; then
	if [ ! -s "$new" ] ; then
	    echo "$new is not empty!"
	    exit -1
	fi
    else
	mkdir -p "$new"
	pushd "$new"
	git init
	popd
    fi
    echo 'git fast-export -all | (cd "$new" && git fast-import)'
    git fast-export --all --progress 1000 | (cd "$new" && git fast-import)
}

	
