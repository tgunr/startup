#
## dumpfile [-w width] [-g group] file
##
##   -w width   Number of bytes per line (default 32)
##   -g group   Number of bytes per "word" group (default 4)
##
#
dumpfile()
{
    local w=32
    local g=4
    
    local usage="usage: dumpfile [-w width] [-g grouping] file"
    local next_arg="" file="" prev_arg
        
    for arg; do
        param=
        case $next_arg in
            -w | --w)
        	w=$arg
    	    	w=${w:=16}
    	    	param=$w
        	next_arg=
        	;;
             -g | --g)
        	g=$arg
    	    	g=${g:=4}
    	    	param=$g
        	next_arg=
        	;;
   	    *)
	      	case $arg in
	      	    -w=* | --w=*)
	      	    	w=`echo $arg | sed -e 's/-*w=//'`
    	    		param=$w
	      	    	;;
	      	    -g=* | --g=*)
	      	    	g=`echo $arg | sed -e 's/-*g=//'`
    	    		param=$g
	      	    	;;
  	    	    -*)
  		    	next_arg=$arg
 		    	;;
  	    	    --*)
  		    	next_arg=$arg
 		    	;;
  		    *)
  		    	if [ "$file" != "" ]; then
  		    	    echo "$usage"
  		    	    echo "Only one file allowed."
  		    	    return 1
  		    	fi
  		    	file="$arg"
  		    	;;
	      	esac
	esac
        if [ "${param:0:1}" = "-" ]; then
        	echo "$usage"
        	echo "$prev_arg missing its parameter($arg seen as its parameter)"
        	$unset_vars
        	return 1
        fi
        prev_arg="$arg"
    done

    if [ "$next_arg" != "" ]; then
	echo "$usage"
    	echo "Unknown option $next_arg"
    	return 1
    fi
   
    if [ "${w//[0-9]/}" != "" ]; then
	echo "$usage"
	echo "Line width must be numeric."
	w=0
    fi
    if [ "${g//[0-9]/}" != "" ]; then
	echo "$usage"
	echo "Group factor must be numeric."
	w=0
    fi
    
    if [ "$file" = "" ]; then
    	echo "$usage"
    	echo "Filename expected."
    	w=0
    fi
    
    if [ $w -eq 0 ]; then
    	return 1;
    fi
    
    # The repetition count is width/group which must be exactly divisible.
    
    declare -i r=$w/$g
    
    if [ $((r*g == w)) != 1 ]; then
    	echo "$usage"
    	echo "Line width must be a multiple of group factor."
    	return 1
    fi
    
    # Since each hex digit takes 2 chars, the format with for each word group is
    # 2 times the grouping factor (e.g., if g is 4, 8 hex digits represent it).
    
    let w2=$((2*g))
    
    if false; then
    	echo "w    = $w"
    	echo "g    = $g"
    	echo "r    = $r"
    	echo "w2   = $w2"
    	echo "file = $file"
    fi
    
    # Try to be a little "fancy" by splitting the display into groups of 16 with
    # an extra space between each group (e.g., if thw width is 32, we want to show
    # 2 groups of 16 with an extra space bwtween the groups).  Hey, why not? :-)
    
    declare -i s=$w/16
    
    if [ $((s*16 == w)) != 1 ] && [ $s -gt 1 ]; then 	# oh well, we tried :-(
	command hexdump -e '"%6_ax: " '$r/$g' "%0'${w2}'x "' -e '" "  '$w'/1 "%_p" "\n"' "$file"
    else					     	# we can do the split
    	let r=$((r/s))					# adjust repetition factor
    	declare -i i=0
    	
    	local e="\"%6_ax: \" $r/$g \"%0${w2}x \""	# generate 1st group of 16
    	
     	let s=s-1					# then all the rest
   	while [ $i -lt $s ]; do
    	    e="$e \"  \" $r/$g \"%0${w2}x \""
    	    let i=$((i+1))
    	done
	
	command hexdump -e "$e" -e '" "  '$w'/1 "%_p" "\n"' "$file" # quoting is not fun!
    fi
}

# Create a new directory and enter it
function mkd() {
	mkdir -p "$@" && cd "$_";
}

# Change working directory to the top-most Finder window location
function cdf() { # short for `cdfinder`
	cd "$(osascript -e 'tell app "Finder" to POSIX path of (insertion location as alias)')";
}

# Create a .tar.gz archive, using `zopfli`, `pigz` or `gzip` for compression
function targz() {
	local tmpFile="${@%/}.tar";
	tar -cvf "${tmpFile}" --exclude=".DS_Store" "${@}" || return 1;

	size=$(
		stat -f"%z" "${tmpFile}" 2> /dev/null; # macOS `stat`
		stat -c"%s" "${tmpFile}" 2> /dev/null;  # GNU `stat`
	);

	local cmd="";
	if (( size < 52428800 )) && hash zopfli 2> /dev/null; then
		# the .tar file is smaller than 50 MB and Zopfli is available; use it
		cmd="zopfli";
	else
		if hash pigz 2> /dev/null; then
			cmd="pigz";
		else
			cmd="gzip";
		fi;
	fi;

	echo "Compressing .tar ($((size / 1000)) kB) using \`${cmd}\`…";
	"${cmd}" -v "${tmpFile}" || return 1;
	[ -f "${tmpFile}" ] && rm "${tmpFile}";

	zippedSize=$(
		stat -f"%z" "${tmpFile}.gz" 2> /dev/null; # macOS `stat`
		stat -c"%s" "${tmpFile}.gz" 2> /dev/null; # GNU `stat`
	);

	echo "${tmpFile}.gz ($((zippedSize / 1000)) kB) created successfully.";
}

# Determine size of a file or total size of a directory
function fs() {
	if du -b /dev/null > /dev/null 2>&1; then
		local arg=-sbh;
	else
		local arg=-sh;
	fi
	if [[ -n "$@" ]]; then
		du $arg -- "$@";
	else
		du $arg .[^.]* ./*;
	fi;
}

# Compare original and gzipped file size
function gz() {
	local origsize=$(wc -c < "$1");
	local gzipsize=$(gzip -c "$1" | wc -c);
	local ratio=$(echo "$gzipsize * 100 / $origsize" | bc -l);
	printf "orig: %d bytes\n" "$origsize";
	printf "gzip: %d bytes (%2.2f%%)\n" "$gzipsize" "$ratio";
}

# Normalize `open` across Linux, macOS, and Windows.
# This is needed to make the `o` function (see below) cross-platform.
if [ ! $(uname -s) = 'Darwin' ]; then
	if grep -q Microsoft /proc/version; then
		# Ubuntu on Windows using the Linux subsystem
		alias open='explorer.exe';
	else
		alias open='xdg-open';
	fi
fi

# `o` with no arguments opens the current directory, otherwise opens the given
# location
function o() {
	if [ $# -eq 0 ]; then
		open .;
	else
		open "$@";
	fi;
}

# `tre` is a shorthand for `tree` with hidden files and color enabled, ignoring
# the `.git` directory, listing directories first. The output gets piped into
# `less` with options to preserve color and line numbers, unless the output is
# small enough for one screen.
function tre() {
	tree -aC -I '.git|node_modules|bower_components' --dirsfirst "$@" | less -FRNX;
}

