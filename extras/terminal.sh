#-----------------------------------------------------------------------------------#

#
## stty options - standard stty command but capture current settings
##
## This "intercepts" the standard stty command so that it can save the current
## stty settings in $STTY.  This is used by the reset command (below) to ensure 
## most recent stty settings are restored when a terminal is reset.
#

function stty()
{
    command stty "$@"
    STTY="`command stty -g`"
}

STTY="`command stty -g &>/dev/null`"

#-----------------------------------------------------------------------------------#

#
## reset - reset terminal (if some output "confuses" the terminal display)
#
function reset()
{
    # reset terminal, reenable wrap
    echo -en '\ec\f\e[?7h'
    stty sane
    stty "$STTY"
}

