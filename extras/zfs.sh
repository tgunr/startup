### ZFS
ZFSBIN=`which zfs`

alias zdz='echo $(date "+%Y%m%d%H%M")'
alias zfsmz='python /usr/local/bin/ZFS-Man.py'
alias zgz="$ZFSBIN get "
alias ziz="$ZFSBIN inherit "
alias zkz="$ZFSBIN destroy "
alias zllz="$ZFSBIN list -o space,mountpoint,creation,pmn:note -s creation  -r -d1 "
alias zlrp='$ZFSBIN list -o name,atime,compression,xattr,acltype,mountpoint,mounted  -r opool'
alias zlsz="$ZFSBIN list -t snapshot -o name,used,creation,pmn:snapnote -s creation -r "
alias zlz="$ZFSBIN list -o name,used,mountpoint,canmount,mounted  -r "
alias zmz="$ZFSBIN mount "
alias znewcz="$ZFSBIN create -o encryption=on -o keylocation=prompt -o keyformat=passphrase "
alias znewpoolz="zpool create -f -o ashift=12 -O xattr=sa -O aclinherit=passthrough -O acltype=posixacl -O atime=off -O normalization=formD -O compression=lz4 -O utf8only=on POOL{^} mirror /dev/disk/by-id/ /dev/disk/by-id/"
alias znewz="$ZFSBIN create -o sharesmb=on -o casesensitivity=insensitive -o normalization=formD -o utf8only=on "
alias znfsz="$ZFSBIN create -o sharenfs=\'rw=@10.1.0.0/22,no_root_squash,no_subtree_check,async,insecure\'  "
alias znotez='$ZFSBIN set pmn:note=""'
alias zpermz="$ZFSBIN allow backup create,destroy,snapshot,rollback,clone,promote,rename,mount,send,receive,quota,reservation,hold "
alias zsetnfsz="$ZFSBIN set sharenfs=\'rw=@10.1.0.0/22,no_root_squash,no_subtree_check,async,insecure\'  "
alias zsmbz="$ZFSBIN set  sharesmb=on "
alias zsmz="$ZFSBIN set mountpoint="
alias zsnapcz='$ZFSBIN snap cpool/ROOT/debian@tm_$(date "+%Y%m%d%H%M") -o pmn:snapnote=""'
alias zsz="$ZFSBIN set "
alias zsz="$ZFSBIN set "
alias zunbindz="mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {}"
alias zuz="$ZFSBIN unmount "
alias zwz="$ZFSBIN get written "

alias zsnapoz='$ZFSBIN snap -r opool@tm_$(date "+%Y%m%d%H%M") -o pmn:snapnote='
alias zoz='opool/ROOT/debian'

function zsnapz {
    if [[ "$1" == "-r" ]]; then
        RECURSE="-r"
        shift
    fi
    if [[ "$2" != "" ]]; then
	    $ZFSBIN snap $RECURSE -o pmn:note=\""$2"\" $1@snap_$(date "+%Y%m%d%H%M")
    else
	    echo "here: $1"
	    $ZFSBIN snap $RECURSE  $1@snap_$(date "+%Y%m%d%H%M")
    fi
    set +x
}

function dehumanize() {
    echo $1 | awk \
      'BEGIN{IGNORECASE = 1}
       function printpower(n,b,p) {printf "%u\n", n*b^p; next}
       /[0-9]$/{print $1;next};
       /K(iB)?$/{printpower($1,  2, 10)};
       /M(iB)?$/{printpower($1,  2, 20)};
       /G(iB)?$/{printpower($1,  2, 30)};
       /T(iB)?$/{printpower($1,  2, 40)};
       /KB$/{    printpower($1, 10,  3)};
       /MB$/{    printpower($1, 10,  6)};
       /GB$/{    printpower($1, 10,  9)};
       /TB$/{    printpower($1, 10, 12)}'
}

function zsendz {
    ZSIZE=$($ZFSBIN list -t snapshot -H -o used $1 | tr '[:upper:]' '[:lower:]')
    if [[ $(echo $ZSIZE | sed -e 's/[a-zA-Z]//') == 0 ]]; then
	PV="pv -p"
    else
	PV="pv -s $(dehumanize $ZSIZE)"
    fi
    $ZFSBIN send $1 | mbuffer  -q -s 128k -m 16M 2>/dev/null | $PV |  $ZFSBIN receive $3 $2
}

export ZFS_ROOT=$(mount|grep -i "on / "|cut -d " " -f1)
