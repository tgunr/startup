
## Create a list of all files recursively seen starting at the specified directory.
## Files with embedded spaces are modified to contain embedded commas.  This is to
## allow such files lists to be used in for-loops.  See quoteFilename below for
## further details.
##
## If the intended list is to be used as an argument list to some other command
## other than a for-loop then the commas need to be filtered out and replaced 
## with escaped spaces.  The following sed filter will do this:
##
## allSourceFiles some-dir | sed -e 's/,/\\ /g'
##
## Note, only non-empty, non-executable (except scripts), and non-symbolic link
# files are output.
#
allSourceFiles()
{
    local dir f1 file
    for f1 in $(ls -1 "$1" | tr ' ' ','); do
    	file=`echo "$f1" | tr ',' ' '`
    	dir="$1/$file"
    	if [ -d "$dir" ]; then
    	    allSourceFiles "$dir"
        elif [ ! -L "$dir" -a -s "$dir" ]; then		# not empty and not link
            if [ ! -x "$dir" ]; then			# not executable
            	if [ "`echo \"$dir\" | sed -e '/[.]o$/D'`" = "" ]; then
            	    if [ "`file \"$dir\" | sed -e '/: ASCII text/D'`" = "" ]; then
            	    	echo "$dir" | tr ' ' ','	# ascii file that ends with .o
            	    fi
            	else					# "normal" source file (I hope)
   	    	    echo "$dir" | tr ' ' ','
   	    	fi
   	    elif [ "`file \"$dir\" | sed -e '/Mach-O/D'`" != "" ]; then # script?
   	    	echo "$dir" | tr ' ' ','
   	    fi
    	fi
    done
}

#-----------------------------------------------------------------------------------#

#
## Convert a filename extracted from the list produced by allSourceFiles into a "usable"
## form.  The list returned by allSourceFiles is a series of filenames (never directories)
## with names containing blanks edited so that the blanks are commas (non-spaces).  The 
## reason for this is that it is assumed that the allSourceFiles list will be used in a
## bash shell for-loop.  The for-loop extracts words (non-blank character sequences). 
## Thus the comma substitution tricks the for-loop to preserve each entire filename
## albeit the blanks replaced with commas.  So the body of the for-loop should pass
## each filename from the list to this quoteFilename to properly quote it and change
## the commas back to blanks again.  All this because brain dead Unix doesn't know
## how to properly handle blanks in filenames :-(
##
## Here's an example use:
##
##   for f in `allSourceFiles xxxx`; do
##       f1="`quoteFilename $f`"
##	...do whatever with $f1...
##   done
#
quoteFilename()
{
    echo -e "\"$1\"" | tr ',' ' '
}

