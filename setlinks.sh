#!/usr/bin/env bash 
#
#	File:		$Id: .setlinks,v 1.3 2004/09/15 01:14:06 davec Exp $
#
#	Contains:	Commands to link startupitems to home directory
#
#	Copyright:	� 2003 by Apple Computer, Inc., all rights reserved.
#
#		$Log: .setlinks,v $
#		Revision 1.3  2004/09/15 01:14:06  davec
#		Update from local.
#
#		Revision 1.2  2004/09/14 19:26:15  davec
#		Updated from local.
#
#		Revision 1.1  2003/04/17 07:45:41  davecl
#		Epoch
#
set -o nounset
set -o errexit

echo "$0 will abort on any error, you will have to manually clean up problems"
STARTDIR=$(dirname "$0")

pushd "$STARTDIR"

source functions/mv-aside

# Transfer all .files to $HOME except for these
for file in .*
do
    # look for exclusions
    if [[ "$file" =~ ".skeleton_startup_file" ]] ; then
	    continue
    fi
    if [ "$file" == "." -o  "$file" == ".." ] ; then
	    continue
    fi
	if [[ "$file" == ".DS_Store" ]] ; then
		continue
	fi
	if [[ ! "${file%%.sh}" ]] ; then
            echo "Skipping: $file"
		continue
	fi
    if [[ "${file:0:2}" == '.#' ]] ; then
	    continue
    fi
    if [[ ! "${file%%.*~}" ]] ; then
	    continue
    fi
    if [[ ! "${file%%.*.exclude}" ]] ; then
	    continue
    fi
    if [[ "${file:0:4}" == ".git" ]] ; then
	    continue
    fi
    case "$file" in
        ("init"|".macos"|".exports"|".functions")
            echo "Skipping: $file"
            continue
            ;;
    esac
    pushd "$HOME" >& /dev/null
    if [[ ! -L "$file" ]] ; then # if no symlink
		if [[ -e "$file" ]] ; then # but it exists
			mv-aside "$file" 
		fi
	elif [[ -L "$file" ]]; then # if symlink does exists
	    rm "$file"
	fi
    echo "$0: creating symlink to $file"
	ln -s  ".startup/$file"
    popd >& /dev/null
done

cd "$HOME"

### Special links
# GIT - the git dot files have to treated special since this folder exists as a repo
if [[ ! -L .gitignore ]] ; then
	if [[ -e .gitignore ]] ; then
		mv-aside .gitignore 
	fi
	# This is special, since git is used inside startup I can't use the default name.
    echo "$0: creating symlink to .gitignore"
    ln  -s .startup/.gitignores .gitignore
fi

if [[ ! -L .gitconfig ]] ; then
	if [[ -e .gitconfig ]] ; then
		mv-aside .gitconfig 
	fi
    echo "$0: creating symlink to .gitconfig"
    ln -s .startup/.gitconfig
fi

if [[ ! -L .gitattributes ]] ; then
	if [[ -e .gitattributes ]] ; then
		mv-aside .gitattributes 
	fi
    echo "$0: creating symlink to .gitattributes"
    ln -s .startup/.gitattributes
fi

if [[ "$SHELL" =~ "bash" ]]; then
if [[ ! -L .git-prompt.conf ]] ; then
	if [[ -e .git-prompt.conf ]] ; then
		mv-aside .git-prompt.conf 
	fi
    echo "$0: creating symlink to .git-prompt.conf"
    ln -s .startup/git-prompt.conf .git-prompt.conf
fi
fi

# Emacs
if [[ ! -L emacs ]] ; then
	if [[ -e emacs ]] ; then
        mv-aside emacs
    fi
    echo "$0: creating symlink to emacs"
    ln -s .startup/emacs
fi

echo "If any of the following exists you may want to compare content to new .startup linked version"
ls -d .*.1 2> /dev/null
ls -d *.1  2> /dev/null

popd

alias cds="cd $STARTDIR"
