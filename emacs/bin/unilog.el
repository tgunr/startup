
(defun unilog ()
"Process unison.log and extract files thet were not copied due to resource forks"
(interactive)
(let ((logfile "~/unison.log"))
  (unless (access-file  logfile "Can't open unison.log")
	(find-file logfile)
	(goto-char 1)
)))