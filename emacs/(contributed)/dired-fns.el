;; dired-fns.el --- Simple functions for opening directories "in place"
;;
;;  Simple Emacs Lisp code to open dired buffers "in place"
;; Steve Kemp <skx@tardis.ed.ac.uk>
;;
;;  Uses the dired function, "dired-get-filename", to
;; get the name of the file at the current point.
;;

(defun dired-open-in-current-buffer ()
  "Open the currently selected directory in the same buffer as this one."
  (interactive)
  (find-alternate-file (dired-get-filename)))

(defun dired-open-directory-in-current-buffer ()
  "Open the file at the current point.
If the selection is a directory then it will be opened in the current buffer,
otherwise it will be opened in a new buffer."
  (interactive)
  (let ((name (dired-get-filename)))
    (save-excursion
      ;; See if the selection is a directory or not.
      (end-of-line)
      (let ((eol (point)))
	(beginning-of-line)
	(if (re-search-forward "^  d" eol t)
	    (find-alternate-file name) ;; Open directory in this buffer
	  (find-file name))))))  ;; Open file in new buffer.
