;; csdiff-setup.el -- csdiff setup

;; Copyright (C) 2000 by David Ponce
;; This file is not part of Emacs

;; Author: David Ponce <david@dponce.com>
;; Maintainer: David Ponce <david@dponce.com>
;; Created: March 15 2000
;; Version: $Revision: 1.2 $
;; Keywords: tools
;; VC: $Id: csdiff-setup.el,v 1.2 2000/10/03 11:07:51 david_ponce Exp $

;; This file is not part of Emacs

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This is the always-loaded portion of csdiff.  It setup csdiff
;; shortcut keys, menu and autoloads

;; Support
;;
;; This program is available at <http://www.dponce.com/>. Any
;; comments, suggestions, bug reports or upgrade requests are welcome.
;; Please send them to David Ponce at <david@dponce.com>

;;; Change Log:

;; $Log: csdiff-setup.el,v $
;; Revision 1.2  2000/10/03 11:07:51  david_ponce
;; Updated documentation to follow Emacs Lisp manual conventions.
;; New `csdiff-folders' command to compare two folders.
;; Some code improvements.
;;
;; Revision 1.1  2000/03/15 15:50:32  david_ponce
;; Initial revision.
;;

;;; Code:
(if (not (eq window-system 'w32))
    (error "The csdiff library requires running %s under w32 window-system"
           (invocation-name)))

(require 'vc)

(defgroup csdiff nil
  "Compare files, folders or versions with CSDiff."
  :group 'tools)

(defcustom csdiff-program "CSDiff.exe"
  "*CSDiff program path."
  :group 'csdiff
  :type '(file :must-match))

(defcustom csdiff-load-hook nil
  "*Hook run when package has been loaded."
  :group 'csdiff
  :type 'hook)

(defvar csdiff-menu
  (list "Compare with CSDiff"
        ["Two Files..."          csdiff-files           t]
        ["Two Folders..."        csdiff-folders         t]
        ["File with Revision..." csdiff-revision        (csdiff-check-if-vc-buffer)]
        ["With Last Version"     csdiff-latest-revision (csdiff-check-if-vc-buffer)]
        )
  "The csdiff menu.")

(defvar csdiff-prefix-map nil
  "The csdiff prefix keymap.")


;;;### (autoloads (csdiff-revision csdiff-latest-revision csdiff-folders
;;;;;;  csdiff-files csdiff-customize) "csdiff" "csdiff.el" (14809
;;;;;;  45833))
;;; Generated autoloads from csdiff.el

(autoload (quote csdiff-customize) "csdiff" "\
Customize csdiff options." t nil)

(autoload (quote csdiff-files) "csdiff" "\
Run CSDiff to compare FILE1 with FILE2." t nil)

(autoload (quote csdiff-folders) "csdiff" "\
Run CSDiff to compare FOLDER1 with FOLDER2." t nil)

(autoload (quote csdiff-latest-revision) "csdiff" "\
Run CSDiff to compare FILE with its most recent checked in version.
Optional argument FILE default to the file visited by the current
buffer." t nil)

(autoload (quote csdiff-revision) "csdiff" "\
Run CSDiff to compare two revisions of FILE.
Optional argument FILE default to the file visited by the current
buffer." t nil)

;;;***

(defun csdiff-check-if-vc-buffer ()
  "Return t if the current buffer visits a version-controlled file."
  (condition-case nil
      (progn
        (vc-ensure-vc-buffer)
        t)
    (error nil)))

;; Add the csdiff menu before the Tools/Compare menu
(if (and (require 'easymenu "easymenu" t) (fboundp 'easy-menu-add-item))
    (easy-menu-add-item nil '("tools") csdiff-menu "compare"))

;; Setup csdiff keys
(setq csdiff-prefix-map (lookup-key global-map "\C-c="))

(if (not (keymapp csdiff-prefix-map))
    (progn
      (setq csdiff-prefix-map (make-sparse-keymap))
      (define-key global-map "\C-c=" csdiff-prefix-map)
      (define-key csdiff-prefix-map "f" 'csdiff-files)
      (define-key csdiff-prefix-map "d" 'csdiff-folders)
      (define-key csdiff-prefix-map "r" 'csdiff-revision)
      (define-key csdiff-prefix-map "=" 'csdiff-latest-revision)))

(provide 'csdiff-setup)

;;; csdiff-setup.el ends here
