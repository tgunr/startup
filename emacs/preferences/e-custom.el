;;$Id: e-custom.el,v 1.1.1.1 2002/08/23 19:46:46 davec Exp $
;;Calls to 'M-x customize' or from menu:
;;	Options->Customize Emacs	(Under 'Help' on old versions)
;;are appended here.
(custom-set-faces
  ;; custom-set-faces was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 )
(custom-set-variables
  ;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(calendar-startup nil)
 '(compilation-scroll-output t)
 '(scalable-fonts-allowed nil)
 '(vc-cvs-global-switches (quote ("-z9")))
 '(vc-cvs-header (quote ("File:		$Id\\$" "" "Contains:	..." "" "Copyright:	� 2003 by Apple Computer, Inc., all rights reserved." "" "	$Log\\$")))
 '(vc-follow-symlinks t)
 '(vc-handled-backends (quote (CVS RCS SCCS)))
 '(vc-mistrust-permissions t))
