#!/bin/bash
# macOS-specific aliases and functions
# Last updated: 2025-02-22

### System Management ###
# System Updates and Maintenance
alias update='sudo softwareupdate -i -a; brew update; brew upgrade; brew cleanup; npm install npm -g; npm update -g; sudo gem update --system; sudo gem update; sudo gem cleanup'
alias flush="dscacheutil -flushcache && killall -HUP mDNSResponder"
alias flushdns='sudo killall -HUP mDNSResponder;sudo killall mDNSResponderHelper;sudo dscacheutil -flushcache'
alias lscleanup="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

# Disk Utilities
alias dut='diskutil '
alias dutm='diskutil mount'
alias dutu='diskutil unmount'
alias dutl='diskutil list '
alias duti='diskutil info '
alias mountefi='diskutil mountdisk disk3'

# Spotlight Control
alias spotoff="sudo mdutil -a -i off"
alias spoton="sudo mdutil -a -i on"

### Finder and UI ###
# Finder Visibility Controls
alias show="defaults write com.apple.finder AppleShowAllFiles -bool true && killall Finder"
alias hide="defaults write com.apple.finder AppleShowAllFiles -bool false && killall Finder"
alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
alias showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"

# File Operations
function trash {
    trashfolder=$HOME/.Trash
    for item in "$@"; do
        if [[ -e $trashfolder/$item ]] ; then
            tempfile=`mktemp /tmp/$item.XXXXXX`
            tempname=`basename $tempfile`
            mv $trashfolder/$item $trashfolder/$tempname
        fi
        mv $item $trashfolder/
    done
}

alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; sudo rm -rfv /private/var/log/asl/*.asl; sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"

### File Management ###
# File Listing and Search
function lsa() {
    order=$1
    folder_path="$1"
    if [ -z "$folder_path" ]; then
        folder_path="."
    fi
    if [ -z "$order" ]; then
        order="-r"
    fi
    find "$folder_path" -type f -exec mdls -name kMDItemDateAdded -name kMDItemFSName {} \; | \
    awk 'BEGIN {FS=" = "}/kMDItemFSName/ {name=$2} /kMDItemDateAdded/ {print $2, name}' | \
    sort $order   
}

alias ltr='ls -ltr'
function mdfind-NSName { FILENAME="$1"; shift; mdfind "kMDItemFSName == \"$FILENAME\"" "$@"; }

# File Attributes
function set_date_added_attribute() {
    set -x
    file_path=$1
    date_added=$(mdls -name kMDItemDateAdded "$file_path" | awk -F'"' '{print $2}')
    formatted_date=$(date -j -f "%Y-%m-%dT%H:%M:%S%z" -d "$date_added" "+%Y-%m-%d")
    xattr -w net.polymicro.date.added "$formatted_date" "$file_path"
}

function unset_date_added_attribute() {
    file_path=$1
    xattr -d net.polymicro.date.added "$file_path"
}

function add_date_added_attribute() {
    folder_path=$1
    find "$folder_path" -type f -depth 1 -exec set_date_added_attribute {} \;
}

### Network ###
# Network Information
alias localip="ipconfig getifaddr en0"
alias ips="ifconfig -a | grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"
alias ifactive="ifconfig | pcregrep -M -o '^[^\t:]+:([^\n]|\n\t)*status: active'"
alias airport='/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport'

# Syncing and Remote Access
alias rsynci='rsync -auv --itemize-changes --out-format="%i %n" --exclude=".DS_Store" --exclude=".git*" --exclude="DerivedData"'
alias synergy-start='/Applications/Synergy.app/Contents/MacOS/synergyc -f --no-tray --name imac --enable-drag-drop 10.1.1.7'
alias dslr='dslrcli-darwin-amd64 -uname tgunr -upassword zW6vKPeqVpJdEp\(f -platform DSL --machinename pro'

### Applications ###
# Browser Controls
alias google='google-chrome --user-data-dir /tmp'
alias chrome='/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome'
alias canary='/Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary'
alias chromekill="ps ux | grep '[C]hrome Helper --type=renderer' | grep -v extension-process | tr -s ' ' | cut -d ' ' -f2 | xargs kill"

# Development Tools
alias h='omz_history'
alias ghce='gh copilot explain'
alias ghcs='gh copilot suggest'
alias dw='/Applications/DeltaWalker.app/Contents/MacOS/DeltaWalker'
alias pstreesp='pstree -sp'
alias uibrowser='rm ~/Library/Logs/K6J1GI\(IkG.plist'

# Emacs Configuration
export EMACSPATH=/Applications/Emacs.app/Contents/MacOS
export PATH=$EMACSPATH:$PATH

function em() {
    if [[ $OSTYPE =~ "darwin" ]]; then 
        socket_file=$(lsof -c Emacs | grep server | tr -s " " | cut -d' ' -f8 | tail -1)
        if [[ $socket_file == "" ]]; then        
            $EMACSPATH/emacs $@ &
        else
            emacsclient $@ -n -s $socket_file
        fi        
    else
        echo "Untested"
        socket_file=$(lsof -c emacs | grep server | tr -s " " | cut -d' ' -f8 | tail -1)
        if [[ $socket_file == "" ]]; then        
            emacs $@ &
        else        
            emacsclient $@ -n -s $socket_file        
        fi
    fi
}

# Quick Look
function qlf() {
    qlmanage -p $1 &>/dev/null
}

# Other Applications
alias m3d='mono /Applications/M3D.app/Contents/Resources/M3DGUI_VS2013.exe'
alias prusacomm='miniterm.py -e /dev/tty.usbmodem344231 115600'
alias openpf='open -a /Applications/Path\ Finder.app/ '
alias plistbuddy="/usr/libexec/PlistBuddy"

### Directory Shortcuts ###
alias cdfw='cd /Volumes/Home/davec/factorwork'
alias cdwork='cd ~/work'

### Factor Development ###
alias ofactor='open /Volumes/Home/davec/factor/Factor.app'
alias mdfactor='mdls -name public_factor_definitions'
alias wwii='open -a /Volumes/Sources/Playnet/wwiiolng/World\ War\ II\ Online.app --args -data 1 -datadir /Volumes/Sources/Playnet/wwiiolng/opt/ww2mods/DATA -logg false -authserver auth.playnet.com'

### System Control ###
alias afk="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"
alias stfu="osascript -e 'set volume output muted true'"
alias pumpitup="osascript -e 'set volume output volume 100'"

### JavaScript ###
# JavaScriptCore REPL
jscbin="/System/Library/Frameworks/JavaScriptCore.framework/Versions/A/Resources/jsc";
[ -e "${jscbin}" ] && alias jsc="${jscbin}";
unset jscbin;
