! -*- factor -*-
USING: accessors colors combinators.short-circuit continuations
debugger fuel.remote init io io.encodings.utf8 io.files io.styles
kernel libc listener math namespaces prettyprint sequences strings
tools.scaffold ui.theme.switching ui.tools.listener unix vectors words ;

t handle-ctrl-break set-global

USE: editors
EDITOR: cursor

! "/Applications/Emacs.app/Contents/MacOS/bin/emacsclient" emacsclient-path set-global
! { "--socket-name=/Users/davec/.emacs.d/server/server"  "--no-wait" } emacsclient-args set-global

IN: tools.scaffold
"Dave Carlton" developer-name set-global

! IN: fuel.remote
! USE: unix
! USE: libc
! USE: combinators.short-circuit
! USE: continuations 
! [ fuel-start-remote-listener* ]
! [ { [ unix-system-call-error? ] [ errno>> EADDRINUSE = ] } 1&&
!   dup [ "Never mind, address already in use" write nl flush ] when ]
! ignore-error

IN: ui.gadgets.editors
+filled+ caret-style set-global

IN: scratchpad
USE: ui.theme.base16
"solarized-dark" base16-theme-name set-global
base16-mode

! ! USE: ui.theme.tango
! tango-dark-mode

! startup-banner
